# Stream Transform
This is a small library that implements the stream transform primitive using CUDA for NVIDIA GPUs. The primitive extends the 
concept of stream compaction to also allow expansion. As this kind of operation is frequently part of and more general 
transformation of the data, the input and output streams are also allowed to be different, which is what motivates the name.
![Stream Transform](doc/images/expansion.png)

## Usage
The library can be called from both host and CUDA code. When called from a .cu file the code is executed on the GPU. The perhaps most intuitive interface uses integer indexes to 
and leaves it up to the user to use these to perform some arbitrary work. Shown below is a toy example that will only work reliably on the CPU, to make it work on the GPU you have to
supply a deterministic replacement for rand(). The examle highlights the fact that you can really use the lambdas to drive arbitrary computations, the key feature is the remapping such 
that the output indices (outInd) are consecutive - enabling coherent writes to memory, or load balancing if the computation is costly. 
```c++
st::transform(0, 64,
	[=]STFM_LAMBDA(st::SizeType ind)
	{
		// expansion ratio between 0 (compaction) and 7 output elements.
		return rand() % 8;
	},
	[=]STFM_LAMBDA(st::SizeType inInd, st::SizeType outInd, st::SizeType subInd)
	{
		printf("%d: %d/%d\n", outInd, inInd, subInd);
	}, nullptr);
```


## Paper
There is a paper describing the primitive and the implementation 'Efficient Stream Transform' to be published at AusPDC 2019, which part of the Australian Computer Science Week 2019 [(ACSW'19)](http://www.acsw.org.au/) conference.

### Limitations
* The library is NOT thread safe.
* The library does not support CUDA streams (this would need more work than just changing the interface!).
* As with all GPU-code different kernels may have very different performance in the real world.

# TODO:

* Implement the cache for transferring data between count and expand. Is it possible to match the predicate functions? Separate entry point?

* Support streams. Streams makes it possible that the function is re-entered while processing on the GPU is in progress (even with just one CPU thread), 
  which makes a lot of things tricky... BufferManager needs to support allocate/free interface, just manage free lists of buffers.

* Make BufferManager singleton a default parameter, and possible pass in, such that the implementation might be made (host) thread safe, or safe assuming the programmer knows what it is doing...

* Tidy up WarpDispatcher - separate repository? The big problem right now is the lack of launch configurations, the number of warps launched is hard-coded at the moment.

* Design method for switching policy / configuration, a struct full of typedefs should do the trick? Consider a way in which we can permute this
  for autotuning, requires compile time generation of all posibilities - struct of type lists?
  * Ordered / unordered.
	* Max expansion factor - used to select optimized implementation paths that have a cap (e.g., naive if <= 4)
	* Enabling / disabling opportunistic optimizations?
	* Also, launch configuration - base off / add dependency on HEMI?.

* Consider using C++17 parallel algorithms to implement for CPU.

# Acknowledgements
