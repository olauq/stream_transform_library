indochina-2004: http://www.cise.ufl.edu/research/sparse/MM/LAW/indochina-2004.tar.gz
europe-osm: http://www.cise.ufl.edu/research/sparse/MM/DIMACS10/europe_osm.tar.gz
roadNet-CA: http://www.cise.ufl.edu/research/sparse/MM/SNAP/roadNet-CA.tar.gz
soc-orkut: http://nrvis.com/download/data/soc/soc-orkut.zip
kron_g500: http://www.cise.ufl.edu/research/sparse/MM/DIMACS10/kron_g500-logn21.tar.gz
nlpkkt: http://www.cise.ufl.edu/research/sparse/MM/Schenk/nlpkkt160.tar.gz
audikw_1: http://www.cise.ufl.edu/research/sparse/MM/GHS_psdef/audikw_1.tar.gz
wikipedia: http://www.cise.ufl.edu/research/sparse/MM/Gleich/wikipedia-20070206.tar.gz
coPapersCiteseer: http://www.cise.ufl.edu/research/sparse/MM/DIMACS10/coPapersCiteseer.tar.gz
hugebubbles: http://www.cise.ufl.edu/research/sparse/MM/DIMACS10/hugebubbles-00020.tar.gz
kkt_power: http://www.cise.ufl.edu/research/sparse/MM/Zaoui/kkt_power.tar.gz

