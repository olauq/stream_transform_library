/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
/****************************************************************************/
#include <inttypes.h>
#include <string>
#include "bfs_example.h"

int main()
{
	const char *fileName = "data/chesapeake.mtx";
	//const char *fileName = "data/roadNet-CA.mtx";
	// ENABLE_CUDA is defined in the detect_cuda.props, it is set to 1 if cuda is present, and 0 otherwise.
	// here we use it to select the CPU version of the code (which is the same code).
#if ENABLE_CUDA
	runBfs_cuda(fileName, 0, false, 10.0f);
#endif //ENABLE_CUDA
	printf("running BFS on CPU:\n");
	runBfs<false>(fileName, 0, false, 10.0f);
}