// Program to print BFS traversal cost from a given source vertex. BFS(int s) 
// traverses vertices reachable from s.

#include <iostream>
#include <iterator>
#include <fstream>
#include <sstream>
#include <list>
#include <climits>
#include <algorithm>
#include <queue>
#include <math.h>
#include "graph.h"

//whether the edges are bidirectional
#define SYMMETRIC

using namespace std;

Graph::Graph(int V)
{
	this->E = 0;
	this->V = V;
	adj = new list<int>[V];
	weight = new vector<int>[V];
}

Graph::~Graph()
{
	delete[] adj;
	delete[] weight;
}

Graph *Graph::create(std::string filename, bool is_weighted)
{
	ifstream ifs;
	stringstream ss;
	string line;
	int n, m, k, w;
	ifs.open(filename, std::ifstream::in);

	if (!ifs.is_open()) {
		cerr << "error opening file\n" << endl;
		return NULL;
	}

	while (1) {
		getline(ifs, line);
		//cout << line << endl; 
		if (line[0] == '%') {
			continue;
		}
		ss << line;
		ss >> n >> m >> k;
		break;
	}

	//cout << "count: " << n << endl;
	Graph *g = new Graph(m);


	while (!ifs.eof())
	{
		getline(ifs, line);

		if (sscanf(line.c_str(), "%d %d %d", &n, &m, &w) == 3)
		{
			g->addEdge(n - 1, m - 1, w);
#ifdef SYMMETRIC
			g->addEdge(m - 1, n - 1, w);
#endif
		}
		else if (sscanf(line.c_str(), "%d %d", &n, &m) == 2)
		{
			//cout << n << ":" << m << endl;
			g->addEdge(n - 1, m - 1, 0);
#ifdef SYMMETRIC
			g->addEdge(m - 1, n - 1, 0);
#endif
		}
		else
		{
		}
	}


	ifs.close();
	return g;
}

void Graph::addEdge(int v, int u, int w)
{
	this->E++;
	adj[v].push_back(u);
	weight[v].push_back(w);
}

void Graph::output()
{
	for (int i = 0; i < V; i++) {
		cout << i << ":  ";
		for (list<int>::iterator j = adj[i].begin(); j != adj[i].end(); ++j) {
			cout << *j << " ";
		}
		cout << endl;
	}
}

float Graph::getAverageDegree()
{
	float sum = 0;
	for (int i = 0; i < V; i++) {
		sum += std::distance(adj[i].begin(), adj[i].end());
	}
	return sum / V;
}

uint32_t Graph::getMaxDegree()
{
	ptrdiff_t maxDeg = 0;
	for (int i = 0; i < V; i++)
	{
		ptrdiff_t count = std::distance(adj[i].begin(), adj[i].end());
		if (count > maxDeg)
		{
			maxDeg = count;
		}
	}
	return uint32_t(maxDeg);
}

std::vector<uint32_t> Graph::getVertexDistribution()
{
	std::vector<uint32_t> distribution;
	distribution.resize(getMaxDegree() + 1, 0);
	for (int i = 0; i < V; i++)
	{
		distribution[std::distance(adj[i].begin(), adj[i].end())]++;
	}
	return distribution;
}

std::vector<uint32_t> Graph::BFS(uint32_t s, uint32_t &edges_visited)
{
	std::vector<bool> visited(V, false);
	std::vector<uint32_t> cost(V, INT_MAX);
	list<int> queue;
	edges_visited = 0;

	visited[s] = true;
	cost[s] = 0;


	queue.push_back(s);

	list<int>::iterator i;

	while (!queue.empty())
	{
		s = queue.front();
		//cout << s << " ";
		queue.pop_front();

		for (i = adj[s].begin(); i != adj[s].end(); ++i)
		{
			if (!visited[*i])
			{
				visited[*i] = true;
				cost[*i] = cost[s] + 1;
				queue.push_back(*i);
			}
		}
		edges_visited += uint32_t(std::distance(adj[s].begin(), adj[s].end()));
	}

	cout << "edges visited: " << edges_visited <<endl;

	return cost;
}

std::vector<uint32_t> Graph::BFS_full_stats(uint32_t s, uint32_t &edges_visited)
{
	std::vector<bool> visited(V, false);
	std::vector<uint32_t> cost(V, INT_MAX);
	list<int> queue;
	edges_visited = 0;
	ptrdiff_t max_degree = 0;

	visited[s] = true;
	cost[s] = 0;


	queue.push_back(s);

	list<int>::iterator i;

	while (!queue.empty())
	{
		s = queue.front();
		//cout << s << " ";
		queue.pop_front();

		for (i = adj[s].begin(); i != adj[s].end(); ++i)
		{
			if (!visited[*i])
			{
				visited[*i] = true;
				cost[*i] = cost[s] + 1;
				queue.push_back(*i);
			}
		}
		ptrdiff_t degree = std::distance(adj[s].begin(), adj[s].end());
		max_degree = degree > max_degree ? degree : max_degree;
		edges_visited += uint32_t(degree);
	}

	cout << "vertices: " << this->getVertices() << endl;
	cout << "edges: " << this->getEdges() << endl;
	cout << "edges visited: " << edges_visited << endl;

	float average = this->getAverageDegree();
	cout << "average degree: " << average  << endl;
	cout << "max_degree: " << max_degree << endl;
	float total = 0;
	for (int i = 0; i < V; i++) {
		total += pow(std::distance(adj[i].begin(), adj[i].end()) - average, 2);
	}

	cout << "standard deviation: " << sqrt(total/V)  << endl;

	return cost;
}

typedef std::pair<uint32_t, uint32_t> pair_T;

class CompareWeight
{
public:
	//a - vertex, b - weight
	bool operator() (pair_T a, pair_T b)
	{
		return a.second < b.second;
	}
};

std::vector<uint32_t> Graph::SSSP(uint32_t s, std::vector<uint32_t> &cost, std::vector<uint32_t> &path)
{
	std::priority_queue<pair_T, std::vector<pair_T>, CompareWeight> pr;
	cost.assign(V, INT_MAX);
	path.assign(V, INT_MAX);
	pair_T edge;
	list<int>::iterator it;
	uint32_t u, w, i;

	pr.push(std::make_pair(s, 0));
	cost[s] = 0;
	path[s] = s;

	while (!pr.empty()) {
		edge = pr.top();
		pr.pop();
		u = edge.first;
		w = edge.second;

		for (it = adj[u].begin(), i = 0; it != adj[u].end(); ++it, ++i) {

			if (cost[u] + weight[u][i] < cost[*it]) {
				cost[*it] = cost[u] + weight[u][i];
				pr.push(std::make_pair(*it, cost[*it]));
				path[*it] = u;
			}
		}
	}
	for (int i = 0; i < V; i++) {
		std::cout << i << " : " << cost[i] << " : "  << path[i] << std::endl;
	}

	return cost;
}

std::vector<uint32_t> Graph::BFS_stats(uint32_t s, uint32_t &edges_visited)
{
	std::vector<bool> visited(V, false);
	std::vector<uint32_t> cost(V, INT_MAX);
	std::vector<uint32_t> vertex_degree(V, 0);
	list<int> queue;
	edges_visited = 0;
	uint32_t max_degree = 0;
	uint32_t vertices_visited = 0;

	visited[s] = true;
	cost[s] = 0;


	queue.push_back(s);

	list<int>::iterator i;

	while (!queue.empty())
	{
		s = queue.front();
		//cout << s << " ";
		queue.pop_front();

		for (i = adj[s].begin(); i != adj[s].end(); ++i)
		{
			if (!visited[*i])
			{
				visited[*i] = true;
				cost[*i] = cost[s] + 1;
				queue.push_back(*i);
			}
		}
		uint32_t degree = uint32_t(std::distance(adj[s].begin(), adj[s].end()));
		vertex_degree[s] = degree;
		max_degree = degree > max_degree ? degree : max_degree;
		edges_visited += degree;
		vertices_visited++;
	}

	cout << "vertices visited: " << vertices_visited << endl;
	cout << "edges: " << this->getEdges() << endl;
	cout << "edges visited: " << edges_visited << endl;

	float average = edges_visited/((float)vertices_visited);

	cout << "average degree: " << average << endl;
	cout << "max_degree: " << max_degree << endl;
	float total = 0;

	for (vector<uint32_t>::iterator it = vertex_degree.begin(); it != vertex_degree.end(); it++) {
		if (*it != 0) {
			total += pow(((float)*it) - average, 2);
		}
	}
	cout << "standard deviation: " << sqrt(total/vertices_visited) << endl;

	return cost;
}

CSR *Graph::toCSR()
{
	CSR *csr = new CSR(getVertices(), getEdges());
	csr->offsets.resize(csr->vertices + 1);
	csr->adj.resize(csr->edges + 1);
	csr->weight.resize(csr->edges + 1);

	int count = 0, i = 0;

	for (i = 0; i < V; i++) {
		csr->offsets[i] = count;
		list<int>::iterator j;
		vector<int>::iterator w;
		for (j = adj[i].begin(), w = weight[i].begin(); j != adj[i].end(); ++j, ++w) {
			csr->adj[count] = *j;
			csr->weight[count] = *w;
			count++;
		}
	}
	csr->offsets[i] = count;
	return csr;
}

void output_CSR(CSR *csr)
{
	cout << "adj: ";
	for (uint32_t i = 0; i < csr->edges + 1; i++) {
		cout << csr->adj[i] << " ";
	}
	cout << endl;
	cout << "offsets: ";
	for (uint32_t i = 0; i < csr->vertices + 1; i++) {
		cout << csr->offsets[i] << " ";
	}
	cout << endl;
	cout << "weight: ";
	for (uint32_t i = 0; i < csr->edges + 1; i++) {
		cout << csr->weight[i] << " ";
	}
	cout << endl;
}

void output_cost(int *cost, int vertices)
{
	cout << "[";
	for (int i = 0; i < vertices; i++) {
		cout << i << ":" << cost[i];
		if (i != vertices - 1) {
			cout << " ";
		}
	}
	cout << "]" << endl;
}
