/****************************************************************************/
/****************************************************************************/

#ifndef _graph_h_
#define _graph_h_

#include <iostream>
#include <list>
#include <vector>

/**
 *
 */
class CSR  
{
public:
	CSR(uint32_t v, uint32_t e) : vertices(v), edges(e) {};

	std::vector<uint32_t> adj;
	std::vector<uint32_t> offsets;
	std::vector<uint32_t> weight;
	uint32_t vertices;
	uint32_t edges;

};

class Graph
{
public:
    static Graph *create(std::string, bool);
    std::vector<uint32_t> getVertexDistribution();
    Graph(int V);  
    ~Graph();
    void addEdge(int, int, int);
    float getAverageDegree();
    uint32_t getMaxDegree();

    std::vector<uint32_t> BFS(uint32_t, uint32_t &);
    std::vector<uint32_t> BFS_stats(uint32_t, uint32_t &);
    std::vector<uint32_t> BFS_full_stats(uint32_t, uint32_t &);
    std::vector<uint32_t> SSSP(uint32_t cost, std::vector<uint32_t> &, std::vector<uint32_t> &);
    int getVertices() {
        return V;
    };
    int getEdges() {
        return E;
    };
    void output();
    CSR *toCSR();

protected:
	//no. of vertices
	int V;
	//no. of edges
	int E;
	std::list<int> *adj;
	std::vector<int> *weight;
};

void output_CSR(CSR *csr);

#endif // _graph_h_
