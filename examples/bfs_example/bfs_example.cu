#include "bfs_example.h"
#include <cuda_runtime_api.h>

#define CUDA_CHECK_ERROR(ASD)

void runBfs_cuda(const std::string &graphFileName, uint32_t startNode, bool randomizeSource, float bufferMult)
{
	cudaDeviceProp devProps;
	cudaGetDeviceProperties(&devProps, 0);

	printf("Running BFS using device '%s'\n", devProps.name);
	runBfs<true>(graphFileName, startNode, randomizeSource, bufferMult);
}
