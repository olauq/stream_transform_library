/****************************************************************************/
/* Copyright (c) 2017-2018, Ola Olsson, Justin Luong
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
/****************************************************************************/
#ifndef _bfs_example_h_
#define _bfs_example_h_

#include <string>
#include <inttypes.h>
#include <limits.h>

#include "../utils/CudaMirrorBuffer.h"
#include "../utils/PerformanceTimer.h"

#include <stream_transform/stream_transform.h>
#include "graph.h"

template <bool IS_GPU>
inline void runBfs(const std::string &graphFileName, uint32_t startNode, bool randomizeSource, float bufferMult)
{
	namespace st = stream_transform;

	// 1. Set up stuff.
	Graph *g = Graph::create(graphFileName.c_str(), 0);
	if (!g)
	{
		return;
	}
	CSR *csr = g->toCSR();
	printf("Loaded graph '%s':\n  verts: %d\n  edges: %d\n", graphFileName.c_str(), csr->vertices, csr->edges);
	fflush(stdout);

	PerformanceTimer pt;
	pt.start();

	uint32_t edges_visited = 0;
	std::vector<uint32_t> cost_cpu = g->BFS_stats(startNode, edges_visited);
	pt.stop();
	std::cout << "CPU elapsed (ms): " << pt.getElapsedTime() * 1000 << std::endl;
	std::cout << "CPU edges/s: " << csr->edges / pt.getElapsedTime() << std::endl;

	const uint32_t V = csr->vertices;

	CudaMirrorBuffer<uint32_t> cost(std::vector<uint32_t>(csr->vertices, INT_MAX));
	CudaMirrorBuffer<uint32_t> adj(csr->adj);
	CudaMirrorBuffer<uint32_t> offsets(csr->offsets);
	uint32_t bufferSize = uint32_t(float(V) * bufferMult);
	int pingPong = 0;
	CudaMirrorBuffer<uint32_t> buffers[2];
	buffers[0].init(bufferSize);
	buffers[0][0] = startNode;
	buffers[1].init(bufferSize);
	if (IS_GPU)
	{
		cost.reflectToDevice(false);
		adj.reflectToDevice(false);
		offsets.reflectToDevice(false);
		buffers[0].reflectToDevice(false);
	}
	auto adjRef = adj.view();
	auto offsetsRef = offsets.view();
	CudaMirrorBuffer<uint32_t> totalBuf(1);

	auto costRef = cost.view();
	uint32_t level = 0;
	uint32_t length = 1;
	pt.start();
	for (; length > 0; ++level)
	{

		//printf("pass: %d, queue: %d\n", level, length);
		auto countPred = [=]STFM_LAMBDA(int v, st::SizeType ind)->uint32_t
		{
			if (costRef[v] > level)
			{
				costRef[v] = level;
				return offsetsRef[v + 1] - offsetsRef[v];
			}
			else
			{
				return 0;
			}
		};

		auto expPred = [=]STFM_LAMBDA(int v, uint32_t i, int subInd)->uint32_t
		{
			return adjRef[offsetsRef[v] + subInd];
		};


		CudaMirrorBuffer<uint32_t> &input = buffers[pingPong];
		CudaMirrorBuffer<uint32_t> &output = buffers[1 - pingPong];
		// Ensure buffers are the right size
		input.resize(length, true, true);
		output.resize(bufferSize, true, true);

		st::transform(input.view(), output.view(), countPred, expPred, totalBuf.ptr());

		if (IS_GPU)
		{
			totalBuf.reflectToHost();
		}
		length = totalBuf[0];

		if (length > output.size())
		{
			fprintf(stderr, "Buffer overflow %u > %u, try increasing bufferMult (%0.2f) and run again\n", length, int(output.size()), bufferMult);
			exit(1);
		}
		pingPong = 1 - pingPong;
	}
	// 3. And the other thingo
	if (IS_GPU)
	{
		cost.reflectToHost();
	}
	pt.stop();
	std::cout << "GPU elapsed (ms): " << pt.getElapsedTime() * 1000 << std::endl;

	std::vector<uint32_t> cost_gpu = cost;

	if (cost_cpu == cost_gpu)
	{
		std::cerr << "CORRECT" << std::endl;
	}
	else
	{
		std::cerr << "INCORRECT" << std::endl;

		std::ostream_iterator<uint32_t> out_it1(std::cerr, ", ");
		std::copy(cost_cpu.begin(), cost_cpu.end(), out_it1);

		std::ostream_iterator<uint32_t> out_it2(std::cerr, ", ");
		std::copy(cost_gpu.begin(), cost_gpu.end(), out_it2);
	}
}

// implemented in bfs_example.cu
void runBfs_cuda(const std::string &graphFileName, uint32_t startNode, bool randomizeSource, float bufferMult);

#endif //_bfs_example_h_
