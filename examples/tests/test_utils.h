/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
/****************************************************************************/
#ifndef _test_utils_h__
#define _test_utils_h__

#include <inttypes.h>
#include <vector>
#include <stream_transform/stream_transform.h>
#include "../utils/CudaMirrorBuffer.h"

enum ValidationType
{
	VT_Compare, // Use when the implementation should produce ordered output.
	VT_SortCompare, // Use for unordered, at which point the result is sorted before comparison. This works for our expand predicate since it 
	                // stores the input number and instance number which should be ordered (see define EXPANSION_PRED_IMPL below),
	                // at least for fewer than N < 24M elements and expansion factors of < 256.
	VT_Max,
};



#define EXPANSION_PRED_IMPL(_input_index_, _sub_index_) ((_input_index_ << 8) | (_sub_index_))

void prettyPrintElement(uint32_t v);
void prettyPrintSolution(const std::vector<uint32_t> &solution);
void prettyPrintInput(const CudaMirrorBuffer<uint32_t> &inData);
void calcReferenceSolution(const CudaMirrorBuffer<uint32_t> &inData, CudaMirrorBuffer<uint32_t> &outData);
void generateUniformRandomTestCase(const int indataSize, const int maxExpansion, const float expansionRate, CudaMirrorBuffer<uint32_t> &inputData, CudaMirrorBuffer<uint32_t> &referenceSolution);
bool compareSolution(CudaMirrorBuffer<uint32_t> &solution, const CudaMirrorBuffer<uint32_t> &referenceSolution, CudaMirrorBuffer<uint32_t> &totalsBuffer, bool printAll, ValidationType validation);

#endif // _test_utils_h__