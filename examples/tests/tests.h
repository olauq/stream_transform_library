#ifndef _tests_h__
#define _tests_h__


void runTests_cuda();

#include <stream_transform/stream_transform.h>
#include "test_utils.h"

inline void printTestResult(const std::string &testDesc, bool result, int n)
{
	printf("Test '%s': %s, N=%d\n", testDesc.c_str(), result ? "PASSED" : "FAILED", n);
}

// entry point for test
template <bool IS_GPU>
int runTests()
{
	printf("Starting tests on %s\n", IS_GPU ? "GPU" : "CPU");
	namespace st = stream_transform;
#ifdef _DEBUG
	const int N = 1 << 10;
#else //!_DEBUG
	const int N = 1 << 20;
#endif // _DEBUG
	{
		CudaMirrorBuffer<uint32_t> inData;
		CudaMirrorBuffer<uint32_t> referenceSolution;
		generateUniformRandomTestCase(N, 32, 0.5f, inData, referenceSolution);
		CudaMirrorBuffer<uint32_t> outData(referenceSolution.size());
		CudaMirrorBuffer<uint32_t> total(1);
		total[0] = 0;

		if (IS_GPU)
		{
			total.reflectToDevice(false);
			inData.reflectToDevice(false);
		}

		st::transform_uo(inData.viewRo(),
			outData.view(),
			[=]STFM_LAMBDA(uint32_t v, uint32_t i) { return v; },
			[=]STFM_LAMBDA(uint32_t v, int i, int subInd) { return EXPANSION_PRED_IMPL(i, subInd); },
			total.ptr());

		if (IS_GPU)
		{
			outData.reflectToHost();
			total.reflectToHost();
		}
		printTestResult("Unordered transform of unisgned integers.", compareSolution(outData, referenceSolution, total, false, VT_SortCompare), N);
	}

	{
		CudaMirrorBuffer<uint32_t> inData;
		CudaMirrorBuffer<uint32_t> referenceSolution;
		generateUniformRandomTestCase(N, 32, 0.5f, inData, referenceSolution);
		CudaMirrorBuffer<uint32_t> outData(referenceSolution.size());
		CudaMirrorBuffer<uint32_t> total(1);
		total[0] = 0;
		CudaMirrorBuffer<uint32_t> inCountBuffer(1);
		inCountBuffer[0] = int(inData.size());

		if (IS_GPU)
		{
			total.reflectToDevice(false);
			inData.reflectToDevice(false);
			inCountBuffer.reflectToDevice(false);
		}
		// This example uses a pointer to the count in 'inCountBuffer' as input size. This can be used to imlpement multi-pass
		// processes without reading back counts to the CPU and thus avoid performance-destroying stalls.
		// NOTE that only the unordeded implementation can currently do this.
		st::transform_uo(st::range::indirect(inData.ptr(), inCountBuffer.ptr()),
			outData.view(),
			[=]STFM_LAMBDA(uint32_t v, uint32_t i) { return v; },
			[=]STFM_LAMBDA(uint32_t v, int i, int subInd) { return EXPANSION_PRED_IMPL(i, subInd); },
			total.ptr());

		if (IS_GPU)
		{
			outData.reflectToHost();
			total.reflectToHost();
		}
		printTestResult("Unordered transform of unisgned integers, using indirect range.", compareSolution(outData, referenceSolution, total, false, VT_SortCompare), N);
	}

	{
		CudaMirrorBuffer<uint32_t> inData;
		CudaMirrorBuffer<uint32_t> referenceSolution;
		generateUniformRandomTestCase(N, 32, 0.5f, inData, referenceSolution);
		CudaMirrorBuffer<uint32_t> outData(referenceSolution.size());
		CudaMirrorBuffer<uint32_t> total(1);
		total[0] = 0;
		if (IS_GPU)
		{
			total.reflectToDevice(false);
			inData.reflectToDevice(false);
		}

		//As above, this example uses a pointer to the count in 'inCountBuffer' as input size. This can be used to imlpement multi-pass
		// processes without reading back counts to the CPU and thus avoid performance-destroying stalls.
		// But this case uses the feature to fetch the size from GPU memory built into the CudaDeviceBufferRef and CudaMirrorBuffer.
		st::transform_uo(inData.viewRo(true),
			outData.view(),
			[=]STFM_LAMBDA(uint32_t v, uint32_t i) { return v; },
			[=]STFM_LAMBDA(uint32_t v, int i, int subInd) { return EXPANSION_PRED_IMPL(i, subInd); },
			total.ptr());

		if (IS_GPU)
		{
			outData.reflectToHost();
			total.reflectToHost();
		}
		printTestResult("Unordered transform of unisgned integers, using indirect range.", compareSolution(outData, referenceSolution, total, false, VT_SortCompare), N);
	}

	{
		CudaMirrorBuffer<uint32_t> inData;
		CudaMirrorBuffer<uint32_t> referenceSolution;
		generateUniformRandomTestCase(N, 32, 0.5f, inData, referenceSolution);
		CudaMirrorBuffer<uint32_t> outData(referenceSolution.size());
		CudaMirrorBuffer<uint32_t> total(1);
		total[0] = 0;
		if (IS_GPU)
		{
			total.reflectToDevice(false);
			inData.reflectToDevice(false);
		}
		st::transform(inData.viewRo(),
			outData.view(),
			[=]STFM_LAMBDA(uint32_t v, uint32_t i) { return v; },
			[=]STFM_LAMBDA(uint32_t v, int i, int subInd) { return EXPANSION_PRED_IMPL(i, subInd); },
			total.ptr());

		if (IS_GPU)
		{
			outData.reflectToHost();
			total.reflectToHost();
		}
		printTestResult("Ordered transform of unisgned integers.", compareSolution(outData, referenceSolution, total, false, VT_Compare), N);
	}
	return 0;
}

#endif // _tests_h__
