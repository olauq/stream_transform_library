/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
/****************************************************************************/
#include "test_utils.h"

#include <inttypes.h>
#include <vector>
#include <algorithm>
#include <numeric>

#define ARRAY_LENGTH(_a_) (sizeof(_a_) / sizeof((_a_)[0]))



void prettyPrintElement(uint32_t v)
{
	int instanceIndex = v & 255;
	int inIndex = v >> 8;
	const char charTab[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	int charInd = inIndex % (ARRAY_LENGTH(charTab) - 1);
	int charInst = inIndex / (ARRAY_LENGTH(charTab) - 1);
	if (charInst > 0)
	{
		printf("%c%d:%d, ", charTab[charInd], charInst, instanceIndex);
	}
	else
	{
		printf("%c:%d, ", charTab[charInd], instanceIndex);
	}
}


void prettyPrintSolution(const CudaMirrorBuffer<uint32_t> &solution)
{
	for (auto v : solution)
	{
		prettyPrintElement(v);
	}
	printf("\n");
}



void prettyPrintInput(const CudaMirrorBuffer<uint32_t> &inData)
{
	for (int i = 0; i < inData.size(); ++i)
	{
		prettyPrintElement((i << 8) | inData[i]);
	}
	printf("\n");
}


void calcReferenceSolution(const CudaMirrorBuffer<uint32_t> &inData, CudaMirrorBuffer<uint32_t> &outData)
{
	uint32_t total = 0;
	for (auto v : inData)
	{
		total += v;
	}
	outData.resize(total);
	uint32_t outLoc = 0;
	for (int i = 0; i < inData.size(); ++i)
	{
		int e = inData[i];
		for (int o = 0; o < e; ++o)
		{
			outData[outLoc++] = EXPANSION_PRED_IMPL(i, o);
		}
	}
}




inline float randomUniform()
{
	return float(rand()) / float(RAND_MAX);
}


/**
* Caculates the expected value of a random variable with the given PDF and possible values that are the same as the index into the PDF.
*/
inline float E(const std::vector<float> &pdf)
{
	float res = 0.0f;
	for (int i = 0; i < int(pdf.size()); ++i)
	{
		res += pdf[i] * float(i);
	}
	return res;
}

/**
* Ensures the PDF sums to 1.0
*/
inline void normalize(std::vector<float> &pdf)
{
	float sum = 0.0f;
	for (int i = 0; i < pdf.size(); ++i)
	{
		sum += pdf[i];
	}
	for (int i = 0; i < pdf.size(); ++i)
	{
		pdf[i] /= sum;
	}
}


/**
* Pick a value in the PDF with uniform distribution...
*/
inline int samplePdf(const std::vector<float> &pdf)
{
	float u = randomUniform();

	for (int i = 0; i < int(pdf.size()); ++i)
	{
		u -= pdf[i];
		if (u <= 0.0f)
		{
			return i;
		}
	}
	// Things may not add up perfectly...
	return int(pdf.size()) - 1;
}



template <typename T>
inline T sgn(T val)
{
	return T((val > T(0)) - (val < T(0)));
}



void generateUniformRandomTestCase(const int indataSize, const int maxExpansion, const float expansionRate, CudaMirrorBuffer<uint32_t> &inputData, CudaMirrorBuffer<uint32_t> &referenceSolution)
{
	float expansion = expansionRate * float(maxExpansion);

	// special case when expansion rate == maxExpansion
	if (fabsf(float(maxExpansion) - expansion) < 0.00001f)
	{
		inputData.resize(indataSize);
		for (auto &&ie : inputData)
		{
			ie = maxExpansion;
		}
	}
	else
	{
		// Generate equal amounts of all
		std::vector<float> pdf(maxExpansion + 1, 1.0f / float(maxExpansion + 1));

		// TODO: Come up with a smarter scheme
		float step = 0.4f;
		float prevDir = E(pdf) - expansion;
		while (fabsf(E(pdf) - expansion) > expansion / 10000.0f)
		{
			// only start reducing step size when goal is passed
			// step = std::min(fabsf(E(pdf) - expansion) / 2.0f, step);
			float dir = E(pdf) - expansion;
			if (sgn(dir) != sgn(prevDir))
			{
				step *= 0.5f;
			}
			prevDir = dir;
			// push up highest and down the rest until the expected value is equal or above the specified value

			if (dir < 0.0f)
			{
				for (int i = 0; i < int(pdf.size()); ++i)
				{
					float f = 1.0f - (step / 2.0f - step * float(i) / float(pdf.size() - 1));
					pdf[i] *= f;
				}
				normalize(pdf);
			}
			// vice versa
			else if (dir > 0.0f)
			{
				for (int i = 0; i < int(pdf.size()); ++i)
				{
					float f = 1.0f + (step / 2.0f - step * float(i) / float(pdf.size() - 1));
					pdf[i] *= f;
				}
				normalize(pdf);
			}
		}

		inputData.resize(indataSize);
		int outputSize = 0;
		for (auto &&ie : inputData)
		{
			int expansion = samplePdf(pdf);
			ie = expansion;
			outputSize += expansion;
		}
	}


	calcReferenceSolution(inputData, referenceSolution);
}


template <typename RANGE_T> 
void sort(RANGE_T &&range)
{
	std::sort(range.begin(), range.end());
}

bool compareSolution(CudaMirrorBuffer<uint32_t> &solution, const CudaMirrorBuffer<uint32_t> &referenceSolution, CudaMirrorBuffer<uint32_t> &totalsBuffer, bool printAll, ValidationType validation)
{
	if (totalsBuffer[0] != referenceSolution.size())
	{
		printf("Solution size mismatch: s: %d != r: %d\n", int(totalsBuffer[0]), int(referenceSolution.size()));
		return false;
	}

	// This is to validate unordered output, to achieve this we just sort the solution as it is supposed to be strictly increasing.
	if (validation == VT_SortCompare)
	{
		sort(solution);
	}

	if (solution.size() != referenceSolution.size())
	{
		printf("Solution size mismatch: s: %d != r: %d\n", int(solution.size()), int(referenceSolution.size()));
		return false;
	}

	bool ok = true;
	int numPrinted = 0;
	const int maxToPrint = 1000;
	for (int i = 0; i < solution.size(); ++i)
	{
		if (!printAll && solution[i] != referenceSolution[i] && numPrinted < maxToPrint)
		{
			printf("[%d]: ", i);
		}
		if (solution[i] != referenceSolution[i] && numPrinted < maxToPrint)
		{
			printf("**");
		}
		if (printAll || solution[i] != referenceSolution[i] && numPrinted < maxToPrint)
		{
			prettyPrintElement(solution[i]);
		}
		if (solution[i] != referenceSolution[i] && numPrinted < maxToPrint)
		{
			printf(" != ");
			prettyPrintElement(referenceSolution[i]);
			printf(" **,  ");
			ok = false;
			++numPrinted;
		}
	}
	if (!ok)
	{
		printf("\n");
	}
	return ok;
}

