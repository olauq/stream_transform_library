#include "tests.h"

#include <vector>
#include <iostream>
//
//template <typename T>
//void foo(T &asd)
//{
//	std::cout << asd[0];
//}
//
//template <typename T>
//void foo(T &&asd)
//{
//	foo(asd);
//}


int main()
{
	//foo(std::vector<int>{10, 11});
	runTests<false>();
#if ENABLE_CUDA
	runTests_cuda();
#endif // ENABLE_CUDA
}