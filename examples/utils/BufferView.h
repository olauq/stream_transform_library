/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
/****************************************************************************/
#ifndef _CudaDeviceBufferRef_h_
#define _CudaDeviceBufferRef_h_

#include "CudaCompat.h"
#include <assert.h>
/**
 * Simple wrapper to pass a buffer to a kernel, since we cannot pass a whole
 * buffer object, for example with pointers to mirror and copy constructors and whatnot.
 * Behaves mostly like a std::vector, but does not manage memory in any way.
 * Supports sub-ranges (but not with stride, at this point).
 */

template <typename T, typename CONTEXT>
class BufferView
{
public:
};



template <typename T>
class BufferView<T, context::cuda>
{
public:
	//CudaDeviceBufferRef() : ptr(nullptr), capacity(0), len(0), sizePtr(nullptr) {}
	using iterator = T*;
	using const_iterator = const T*;

	T * CUDA_RESTRICT ptr;
	uint32_t *sizePtr;
	size_t len;
	size_t capacity;

	FUNC_CUDA_D T &operator[](int index) const
	{
		assert(index >= 0);
		assert(index < int(size()));
		return ptr[index];
	}

	FUNC_CUDA_HD T *CUDA_RESTRICT begin() const
	{
		return ptr;
	}
	FUNC_CUDA_HD T *CUDA_RESTRICT end() const
	{
		return ptr + size();
	}

	// NOTE: returns the dynamic size if present!
	FUNC_CUDA_HD size_t size() const { return sizePtr ? *sizePtr : len; }

	// NOTE: sub-section cannot access dynamic size!!
	FUNC_CUDA_HD BufferView<T, context::cuda> sub(size_t offset, size_t count)
	{
		assert(offset < size());
		assert(offset + count <= size());
		return BufferView<T, context::cuda>{ ptr + offset, nullptr, count, count };
	}
#ifdef __CUDA_CC__
	FUNC_CUDA_D void push_back(const T &v) const
	{
		assert(sizePtr);
		assert(size() < capacity);
		uint32_t offset = atomicAdd(sizePtr, 1);
		ptr[offset] = v;
	}

	FUNC_CUDA_D uint32_t allocateRange(uint32_t count) const
	{
		assert(sizePtr);
		assert(size() + count <= capacity);
		return atomicAdd(sizePtr, count);
	}
#endif // __CUDA_CC__
	// NOTE: non-atomic user is responsible for that bit...
	FUNC_CUDA_D void setSize(uint32_t newSize)
	{
		assert(newSize <= capacity);
		if (sizePtr)
		{
			*sizePtr = newSize;
		}
		else
		{
			len = newSize;
		}
	}
};


template <typename T>
class BufferView<T, context::host>
{
public:
	//CudaDeviceBufferRef() : ptr(nullptr), capacity(0), len(0), sizePtr(nullptr) {}
	using iterator = T*;
	using const_iterator = const T*;

	T * ptr;
	uint32_t *sizePtr;
	size_t len;
	size_t capacity;

	T &operator[](int index) const
	{
		assert(index >= 0);
		assert(index < int(size()));
		return ptr[index];
	}

	T * begin() const
	{
		return ptr;
	}
	T * end() const
	{
		return ptr + size();
	}

	// NOTE: returns the dynamic size if present!
	size_t size() const { return sizePtr ? *sizePtr : len; }

	// NOTE: sub-section cannot access dynamic size!!
	BufferView<T, context::host> sub(size_t offset, size_t count)
	{
		assert(offset < size());
		assert(offset + count <= size());
		return BufferView<T, context::host>{ ptr + offset, nullptr, count, count };
	}

	void push_back(const T &v) const
	{
		assert(sizePtr);
		assert(size() < capacity);
		uint32_t offset = allocateRange(1);
		ptr[offset] = v;
	}

	uint32_t allocateRange(uint32_t count) const
	{
		uint32_t prevSize = size();
		setSize(size() + count);
		return prevSize;
	}

	// NOTE: non-atomic user is responsible for that bit...
	void setSize(uint32_t newSize)
	{
		assert(newSize <= capacity);
		if (sizePtr)
		{
			*sizePtr = newSize;
		}
		else
		{
			len = newSize;
		}
	}
};



#endif // _CudaDeviceBufferRef_h_
