/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
/****************************************************************************/
#ifndef _CudaRuntimeApiWrapper_h_
#define _CudaRuntimeApiWrapper_h_

#include <stdlib.h>
// The main purpose of this namespace and functions is to replicate the cuda api functions that I happen to need
// but without exposing the global namespace to the whole of the cuda runtime API, which notably includes 'float3' for
// name-clash galore (@nvidia how about a 'cuda' namespace??)!
//
// In also provides the host-side APIs when there is no CUDA present (at compile time, or if detectCudaDevice() is called and returned false also at runtime)
// , to make it easy to use for example CudaMirrorBuffer
//

namespace cuda_wrapper
{
bool detectCudaDevice();

struct EventSt;
using Event_t = EventSt*;

void EventCreate(Event_t *e);
void EventDestroy(Event_t e);
void EventSynchronize(Event_t e);

void Free(void *ptr);
void FreeHost(void *ptr);

enum MemcpyKind
{
	MemcpyHostToHost = 0,      /**< Host   -> Host */
	MemcpyHostToDevice = 1,      /**< Host   -> Device */
	MemcpyDeviceToHost = 2,      /**< Device -> Host */
	MemcpyDeviceToDevice = 3,      /**< Device -> Device */
	MemcpyDefault = 4       /**< Direction of the transfer is inferred from the pointer values. Requires unified virtual addressing */
};

void Memcpy(void *dst, const void *src, size_t count, enum MemcpyKind kind);
void MemcpyAsync(void *dst, const void *src, size_t count, enum MemcpyKind kind);
void Malloc(void **p, size_t s);


constexpr unsigned int HostAllocDefault = 0x00;  /**< Default page-locked allocation flag */
constexpr unsigned int HostAllocPortable = 0x01;  /**< Default page-locked allocation flag */
constexpr unsigned int HostAllocMapped = 0x02;  /**< Default page-locked allocation flag */
constexpr unsigned int HostAllocWriteCombined = 0x04;  /**< Default page-locked allocation flag */

void HostAlloc(void **pHost, size_t size, unsigned int flags = HostAllocDefault);

}; // namespace cuda_wrapper

#endif // _CudaRuntimeApiWrapper_h_
