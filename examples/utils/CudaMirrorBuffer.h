/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
/****************************************************************************/
#ifndef _CudaMirrorBuffer_h_
#define _CudaMirrorBuffer_h_

#include <cstdio>
#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>

#include <assert.h>
#include <memory.h>

#include "CudaCompat.h"
#include "CudaRuntimeApiWrapper.h"
#include "BufferView.h"

// TODO: Add support for streams
// TODO: Add support for unified memory
// TODO?: Maybe add modification tracking? Maybe not/
/**
 * CudaMirrorBuffer is similar to std::vector, but supports reflecting the data to and from the CUDA managed GPU memory.
 * The reflection is explicit and the responsibility of the programmer, which is geared towards enabling high performance.
 * Note: only works for trivial types (because it copies memory in bulk).
 */
template <typename T>
class CudaMirrorBuffer
{
public:
	static_assert(std::is_trivial<T>::value, "T must be a trivial type, as CudaMirrorBuffer manages memory in such a way");
	typedef T *PtrType;

	// these should not be accessed from device code
	FUNC_CUDA_H CudaMirrorBuffer(size_t size = 0) :
		m_data(0),
		m_hostData(0),
		m_asyncInProgress(false)
	{ 
#if ENABLE_CUDA
		cuda_wrapper::EventCreate(&m_asyncCopyEvent);
#endif // ENABLE_CUDA
		init(size);
	} 

	FUNC_CUDA_H CudaMirrorBuffer(const T *hostData, size_t size) :
		m_data(0),
		m_hostData(0),
		m_asyncInProgress(false)
	{ 
#if ENABLE_CUDA
		cuda_wrapper::EventCreate(&m_asyncCopyEvent);
#endif // ENABLE_CUDA
		init(hostData, size);
		//copyFromHost(size);
	} 
	FUNC_CUDA_H CudaMirrorBuffer(const std::vector<T> &data) :
		m_data(0),
		m_hostData(0),
		m_asyncInProgress(false)
	{
#if ENABLE_CUDA
		cuda_wrapper::EventCreate(&m_asyncCopyEvent);
#endif // ENABLE_CUDA
		if (!data.empty())
		{
			init(data.data(), data.size());
		}
	}

#if 0
	CudaMirrorBuffer(const CudaMirrorBuffer &other) : 
		m_size(0), 
		m_data(0),
		m_hostData(0),
		m_asyncInProgress(false)
	{
		cuda_wrapper::EventCreate(&m_asyncCopyEvent);

		init(other.capacity());
		copy(other, other.size());
	}
#endif

	FUNC_CUDA_H ~CudaMirrorBuffer()
	{
#if ENABLE_CUDA
		cuda_wrapper::EventDestroy(m_asyncCopyEvent);
#endif // ENABLE_CUDA
		clear(true);
	}

	T *data() 
	{
		//assert(m_hostData);
		// return pointer to just beyond meta data block
		return m_hostData ? reinterpret_cast<T*>(m_hostData + 1) : nullptr;
	}
	const T *data() const
	{
		//assert(m_hostData);
		// return pointer to just beyond meta data block
		return m_hostData ? reinterpret_cast<T*>(m_hostData + 1) : nullptr;
	}
	
	// Begin / end or [] access is all host-side
	T &operator[](size_t index)
	{
		assert(!m_asyncInProgress);
		assert(index < size());
		return data()[index];
	}

	const T &operator[](size_t index) const
	{
		assert(!m_asyncInProgress);
		assert(index < size());
		return data()[index];
	}


	const T *begin() const { return data(); }
	const T *end() const { return data() + size(); }
	T *begin() { return data(); }
	T *end() { return data() + size(); }

	const T *cudaPtr() const { return m_data ? reinterpret_cast<T*>(m_data + 1) : nullptr; }
	T *cudaPtr() { return m_data ? reinterpret_cast<T*>(m_data + 1) : nullptr; }

	// Selective pointer accessors that resolve:
	// From a .cu file (where __CUDA_CC__ is defined) it returns a device pointer, 
	// from a cpp file (where it isnt), they return a view of the host pointer.
	// This makes it easier to work with the stream transform that will similarly invoke a kernel if used in a .cu file
	// or call a host function if called on the CPU.

	// I'm pretty sure this can be done in a neater way... but how?

	const T *internal_ptr(const compilation_context::cuda &) const { return cudaPtr(); }
	const T *internal_ptr(const compilation_context::host &) const { return data(); }

	template <typename CC = compilation_context::default_context>
	const T *ptr() const
	{
		return internal_ptr(CC());
	}


	T *internal_ptr(const compilation_context::cuda &) { return cudaPtr(); }
	T *internal_ptr(const compilation_context::host &) { return data(); }


	template <typename CC = compilation_context::default_context>
	T *ptr()
	{
		return internal_ptr(CC());
	}

	BufferView<T, context::cuda> view(const compilation_context::cuda&, bool dynamicSize)
	{
		return deviceView(dynamicSize);
	}

	BufferView<T, context::host> view(const compilation_context::host&, bool dynamicSize)
	{
		return BufferView<T, context::host> { data(), (m_hostData && dynamicSize) ? &m_hostData->size : nullptr, size(), capacity() };
	}


	template <typename CC = compilation_context::default_context>
	BufferView<T, CC> view(bool dynamicSize = false)
	{
		return view(CC(), dynamicSize);
	}

	// the view functions selects the memory space based on how it is being compiled:
	// From a .cu file (where __CUDA_CC__ is defined) it returns a device view, 
	// from a cpp file (where it isnt), they return a view of the host buffer.

	BufferView<const T, context::cuda> view(const compilation_context::cuda&, bool dynamicSize) const
	{
		return deviceView(dynamicSize);
	}

	BufferView<const T, context::host> view(const compilation_context::host&, bool dynamicSize) const
	{
		return BufferView<const T, context::host> { data(), (m_hostData && dynamicSize) ? &m_hostData->size : nullptr, size(), capacity() };
	}

	template <typename EC = compilation_context::default_context>
	BufferView<const T, EC> view(bool dynamicSize = false) const
	{
		return view(EC(), dynamicSize);
	}

	template <typename EC = compilation_context::default_context>
	BufferView<const T, EC> viewRo(bool dynamicSize = false) const
	{
		return view(EC(), dynamicSize);
	}

		void push_back(const T &v)
	{
		assert(size() < capacity());
		data()[m_hostData->size++] = v;
	}

	// Note: only resets the size counter, unless 'freeMem' is true
	void clear(bool freeMem = false)
	{
		if (freeMem)
		{
			reserve(0U);
		}
		else
		{
			resize(0, true);
		}
	}

	void init(size_t size)
	{
		resize(size);
	}

	void init(const T *hostData, size_t size)
	{
		reserve(size);
		copyFromHost(hostData, size);
	}
	
	// Copy size from the other one on both host & device side, performs no allocation, requres that the destination has a greater or equal capacity
	void copySize(const CudaMirrorBuffer<T> &src)
	{
		assert(capacity() >= src.capacity());
		if (m_hostData)
		{
			m_hostData->size = uint32_t(src.size());
		}
		if (m_data && src.m_data)
		{
			cuda_wrapper::Memcpy(&m_data->size, &src.m_data->size, sizeof(src.m_data->size), cuda_wrapper::MemcpyDeviceToDevice);
		}
	}

	void resize(size_t newSize, bool growOnly = false, bool keepData = false)
	{
		reserve(newSize, growOnly, keepData);
		assert(newSize <= capacity());
		assert(newSize <= std::numeric_limits<uint32_t>::max());
		assert(m_hostData || newSize == 0);
		if (m_hostData)
		{
			m_hostData->size = uint32_t(newSize);
		}
		reflectMetaDataToDevice(true);
	}

	// TODO: add stream support
	void reserve(size_t _capacity, bool growOnly = false, bool keepData = false)
	{
		// internally we ues only 32 bit values because of atomic limitations.
		assert(_capacity <= std::numeric_limits<uint32_t>::max());
		uint32_t newCapacity = uint32_t(_capacity);

		if (!growOnly || newCapacity > capacity())
		{
			MetaData *oldData = nullptr;
			MetaData *oldHostData = nullptr;
			uint32_t oldSize = 0U;
			if (m_data)
			{
				// defer freeing of data...
				if (keepData && newCapacity > 0)
				{
					oldData = m_data;
					oldHostData = m_hostData;
					oldSize = m_hostData->size;
				}
				else
				{
					cuda_wrapper::Free(m_data);
					cuda_wrapper::FreeHost(m_hostData);
				}
				m_hostData = nullptr;
				m_data = nullptr;
			}
			//m_hostData->size = 0;
			//m_hostData->capacity = capacity;
			if (newCapacity)
			{
				{
					void *tmp = nullptr;
					cuda_wrapper::HostAlloc(&tmp, sizeof(MetaData) + newCapacity * sizeof(T), cuda_wrapper::HostAllocDefault);
					assert(tmp);
					m_hostData = static_cast<MetaData*>(tmp);
				}
				m_hostData->capacity = newCapacity;
				m_hostData->size = 0;
				{
					void *tmp = nullptr;
					cuda_wrapper::Malloc(&tmp, sizeof(MetaData) +  newCapacity * sizeof(T));
					// Allowed to be NULL as there may be no CUDA... assert(tmp);
					m_data = static_cast<MetaData*>(tmp);
				}
			}
			// preserve data and deallocate
			if (oldData)
			{
				m_hostData->size = std::min(oldSize, newCapacity);
				// copy the device side data (but not meta-data -> which may now be out of sync.
				cuda_wrapper::Memcpy(cudaPtr(), oldData + 1, m_hostData->size * sizeof(T), cuda_wrapper::MemcpyDeviceToDevice);
				cuda_wrapper::Free(oldData);

				// copy the host side data (but not meta-data -> which may now be out of sync.
				cuda_wrapper::Memcpy(data(), oldHostData + 1, m_hostData->size * sizeof(T), cuda_wrapper::MemcpyHostToHost);
				cuda_wrapper::FreeHost(oldHostData);
			}

			// TODO: this is not actually needed for example for the host-side copy operations (as they imply the user will have to sync later anyway)
			// since the user is responsible for managing the data reflections we don't know if they are intending to use/update the device side or 
			// host-side data thus we have to update the device side meta-data, just in case. NOTE: this must be done in the correct stream.  
			reflectMetaDataToDevice(true);
		}
	}

	// needs to get called whenever the meta data changes on the host in such a way that it is not obvious that a reflect will happen later
	// e.g, resize... (otoh push_back implies it is the users responsibility).
	void reflectMetaDataToDevice(bool async = false)
	{
		if (m_hostData && m_data)
		{
			if (async)
			{
				cuda_wrapper::MemcpyAsync(m_data, m_hostData, sizeof(MetaData), cuda_wrapper::MemcpyHostToDevice);
			}
			else
			{
				cuda_wrapper::Memcpy(m_data, m_hostData, sizeof(MetaData), cuda_wrapper::MemcpyHostToDevice);
			}
		}
	}

	bool empty() const
	{
		return size() == 0;
	}

	size_t size() const
	{
		return m_hostData ? m_hostData->size : 0U;
	}

	size_t byteSize() const
	{
		return size() * sizeof(T);
	}

	size_t capacity() const
	{
		return m_hostData ? m_hostData->capacity : 0U;
	}

	size_t byteCapacity() const
	{
		return capacity() * sizeof(T);
	}

	BufferView<T, context::cuda> deviceView(bool dynamicSize = false)
	{
		return BufferView<T, context::cuda> { cudaPtr(), (m_data && dynamicSize) ? &m_data->size : nullptr, size(), capacity() };
	}

	
	BufferView<const T, context::cuda> deviceView(bool dynamicSize = false) const
	{
		return BufferView<const T, context::cuda> { cudaPtr(), (m_data && dynamicSize) ? &m_data->size : nullptr, size(), capacity() };
	}

	
	BufferView<const T, context::cuda> deviceViewRo(bool dynamicSize = false) const
	{
		return BufferView<const T, context::cuda> { cudaPtr(), (m_data && dynamicSize) ? &m_data->size : nullptr, size(), capacity() };
	}



	void set(uint8_t value, size_t count = ~size_t(0))
	{
		memset(data(), value, std::min(count, size()) * sizeof(T));
		// TODO: should really do cuda-side set also to ensure consistent views (?)...
		// TODO: add flags to select either or both?
	}

	/**
	 * Transfer data to device
	 * TODO: add support for async & streams.
	 */
	void reflectToDevice(bool async)
	{
		// Does nothing if CUDA is not present.
#if ENABLE_CUDA
		if (m_hostData && m_data)
		{
			if (async)
			{
				cuda_wrapper::MemcpyAsync(m_data, m_hostData, sizeof(MetaData) + m_hostData->size * sizeof(T), cuda_wrapper::MemcpyHostToDevice);
			}
			else
			{
				cuda_wrapper::Memcpy(m_data, m_hostData, sizeof(MetaData) + m_hostData->size * sizeof(T), cuda_wrapper::MemcpyHostToDevice);
			}
		}
#endif  // ENABLE_CUDA
	}

	/**
	* Transfer data to host
	* TODO: add support for async.
	*/
	void reflectToHost()
	{
#if ENABLE_CUDA
		if (m_hostData && m_data)
		{
			cuda_wrapper::Memcpy(m_hostData, m_data, sizeof(MetaData) + m_hostData->capacity * sizeof(T), cuda_wrapper::MemcpyDeviceToHost);
		}
#endif  // ENABLE_CUDA
	}

	/**
	 * Copies data on both host and device sides from 'sourceBuffer'
	 * TODO: add version with host/device side flags?
	 * Automatically clamps to size of source buffer.
	 */
	void copy(const CudaMirrorBuffer<T> &sourceBuffer)//, size_t count = ~size_t(0))
	{
		size_t num = sourceBuffer.size();
		assert(capacity() >= num);

		memcpy(m_data, sourceBuffer.m_data, sizeof(MetaData) + num * sizeof(T));
#if ENABLE_CUDA
		cuda_wrapper::Memcpy(m_hostData, sourceBuffer.m_hostData, sizeof(MetaData) + num * sizeof(T), cuda_wrapper::MemcpyDeviceToDevice);
#endif // ENABLE_CUDA
		// it would be weird otherwise
		assert(m_hostData->size == num);
	}

	/**
	* Copies from a host data pointer to the host-side mirror buffer,
	* NOTE: Does not reflect to Device buffer!
	*/
	void copyFromHost(const T *srcData, size_t count)
	{
		assert(capacity() >= count);
		assert(count <= std::numeric_limits<uint32_t>::max());
		//cuda_wrapper::Memcpy(m_data, data, count * sizeof(T), cuda_wrapper::MemcpyHostToDevice);
		memcpy(data(), srcData, count * sizeof(T));
		m_hostData->size = uint32_t(count);
	}

#if 0
	/**
	 * Copies from a device data pointer to the device-side mirror buffer, 
	 * NOTE: Does not reflect to Host buffer!
	 */
	void copyFromDevice(const T *deviceData, size_t count)
	{
		TODO: must be very careful with device side maintained meta data
		assert(m_hostData->capacity >= count);
		cuda_wrapper::Memcpy(m_data, deviceData, count * sizeof(T), cuda_wrapper::MemcpyDeviceToDevice);
		m_hostData->size = count;
	}
#endif
	/**
	 * Ensure async copies have finished.
	 */
	void sync()
	{
#if ENABLE_CUDA
		if (m_asyncInProgress)
		{
			cuda_wrapper::EventSynchronize(m_asyncCopyEvent);
			m_asyncInProgress = false;
		}
#endif // ENABLE_CUDA
	}

	void operator=(const std::vector<T> &other)
	{
		reserve(other.size(), true);
		if (!other.empty())
		{
			copyFromHost(other.data(), other.size());
		}
	}
	operator std::vector<T>() const
	{
		return std::vector<T>(data(), data() + size());
	}

private:
	// These are in their own struct to make replication to and from device easier (and more obvious)
	struct MetaData
	{
		uint32_t size;
		uint32_t capacity;
		// pad to 64 byte boundary
		uint32_t _padding[14];
	};
	static_assert(sizeof(MetaData) == 64U, "Invalid metadata struct size");

	// Note: we don't compile out the data declarations even when CUDA is not present
	// as this avoids annoying bugs if 'ENABLE_CUDA' is defined inconsistently.
	cuda_wrapper::Event_t m_asyncCopyEvent;
	bool m_asyncInProgress;
	// NOTE: the data arrays (m_data / m_hostData) both start with the metadata block
	// (MetaData  T[capacity])
	MetaData *m_data;
	MetaData *m_hostData;
};

#endif // _CudaMirrorBuffer_h_
