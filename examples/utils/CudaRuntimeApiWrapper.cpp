/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
/****************************************************************************/
#include "CudaRuntimeApiWrapper.h"
#include <inttypes.h>
#include <assert.h>
#include <stdlib.h>
#include <memory.h>

#if ENABLE_CUDA
#include <cuda_runtime_api.h>
#endif // ENABLE_CUDA

#define CHECK_CONSTANT(X) static_assert(uint32_t(X)==uint32_t(cuda##X), "Enums out of sync")
#define CHECK_SIZE(X) static_assert(sizeof(X)==sizeof(cuda##X), "Size out of sync")

namespace cuda_wrapper
{

#if ENABLE_CUDA
static bool g_cudaDevicePresent = true;
#else // !ENABLE_CUDA
	static bool g_cudaDevicePresent = false;
#endif // ENABLE_CUDA

bool detectCudaDevice()
{
#if ENABLE_CUDA
	int deviceCount = 0;
	g_cudaDevicePresent = cudaGetDeviceCount(&deviceCount) == cudaSuccess && deviceCount > 0;
#endif // ENABLE_CUDA
	return g_cudaDevicePresent;
}
	
	
void EventCreate(Event_t *e)
{
#if ENABLE_CUDA
	if (g_cudaDevicePresent)
	{
		CHECK_SIZE(Event_t);

		cudaEvent_t *ee = reinterpret_cast<cudaEvent_t*>(e);
		cudaEventCreate(ee);
	}
	else
#endif // ENABLE_CUDA
	{
		e = nullptr;
	}
}

void EventDestroy(Event_t e)
{
#if ENABLE_CUDA
	if (g_cudaDevicePresent)
	{
		CHECK_SIZE(Event_t);

		cudaEvent_t ee = reinterpret_cast<cudaEvent_t>(e);
		cudaEventDestroy(ee);
	}
#endif // ENABLE_CUDA
}

void EventSynchronize(Event_t e)
{
#if ENABLE_CUDA
	if (g_cudaDevicePresent)
	{
		CHECK_SIZE(Event_t);

		cudaEvent_t ee = reinterpret_cast<cudaEvent_t>(e);
		cudaEventSynchronize(ee);
	}
#endif // ENABLE_CUDA
}


void Free(void *ptr)
{
#if ENABLE_CUDA
	if (g_cudaDevicePresent)
	{
		cudaFree(ptr);
	}
	else
#endif // ENABLE_CUDA
	{
		assert(!ptr);
	}
}

void FreeHost(void *ptr)
{
#if ENABLE_CUDA
	if (g_cudaDevicePresent)
	{
		cudaFreeHost(ptr);
	}
	else
#endif // ENABLE_CUDA
	{
		free(ptr);
	}
}


void Memcpy(void *dst, const void *src, size_t count, enum MemcpyKind kind) 
{
#if ENABLE_CUDA
	CHECK_CONSTANT(MemcpyHostToHost);
	CHECK_CONSTANT(MemcpyHostToDevice);
	CHECK_CONSTANT(MemcpyDeviceToHost);
	CHECK_CONSTANT(MemcpyDeviceToDevice);
	CHECK_CONSTANT(MemcpyDefault);
	if (g_cudaDevicePresent)
	{

		cudaMemcpy(dst, src, count, cudaMemcpyKind(kind));
	}
	else
#endif // ENABLE_CUDA
	{
		if (kind == MemcpyHostToHost || kind == MemcpyDefault)
		{
			memcpy(dst, src, count);
		}
		else
		{
			assert(!dst || !src); // one or the other or both should be NULL if in CUDA-free mode (and they are device pointers).
		}
	}
}

void MemcpyAsync(void *dst, const void *src, size_t count, enum MemcpyKind kind)
{
#if ENABLE_CUDA
	CHECK_CONSTANT(MemcpyHostToHost);
	CHECK_CONSTANT(MemcpyHostToDevice);
	CHECK_CONSTANT(MemcpyDeviceToHost);
	CHECK_CONSTANT(MemcpyDeviceToDevice);
	CHECK_CONSTANT(MemcpyDefault);
	if (g_cudaDevicePresent)
	{
		cudaMemcpyAsync(dst, src, count, cudaMemcpyKind(kind));
	}
	else
#endif // ENABLE_CUDA
	{
		if (kind == MemcpyHostToHost || kind == MemcpyDefault)
		{
			memcpy(dst, src, count);
		}
		else
		{
			assert(!dst || !src); // one or the other or both should be NULL if in CUDA-free mode (and they are device pointers).
		}
	}
}

void HostAlloc(void **pHost, size_t size, unsigned int flags)
{
#if ENABLE_CUDA
	CHECK_CONSTANT(HostAllocDefault);
	CHECK_CONSTANT(HostAllocPortable);
	CHECK_CONSTANT(HostAllocMapped);
	CHECK_CONSTANT(HostAllocWriteCombined);

	if (g_cudaDevicePresent)
	{
		cudaHostAlloc(pHost, size, flags);
	}
	else
#endif // ENABLE_CUDA
	{
		*pHost = malloc(size);
	}
}

void Malloc(void **p, size_t s)
{
#if ENABLE_CUDA
	if (g_cudaDevicePresent)
	{
		cudaMalloc(p, s);
	}
	else
#endif // ENABLE_CUDA
	{
		*p = nullptr;
	}
}

}; // namespace cuda_wrapper

