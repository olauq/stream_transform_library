/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
/****************************************************************************/
#ifndef _CudaCompatUtils_h_
#define _CudaCompatUtils_h_

namespace context
{
	struct cuda
	{
	};

	struct host
	{
	};
}
namespace compilation_context
{
	using host = context::host;
	using cuda = context::cuda;
};

#ifdef __CUDACC__
namespace compilation_context
{
	using default_context = cuda;
}
#include <cuda_runtime.h>

#define FUNC_CUDA_HD inline __device__ __host__
#define FUNC_CUDA_D inline __device__ 
#define FUNC_CUDA_H inline __host__ 
#define CUDA_RESTRICT __restrict__


#else // !__CUDACC__

namespace compilation_context
{
	using default_context = host;
}

#define FUNC_CUDA_HD inline
#define FUNC_CUDA_D inline 
#define FUNC_CUDA_H inline 
#define CUDA_RESTRICT 

#endif // __CUDACC__

#endif // _CudaCompatUtils_h_
