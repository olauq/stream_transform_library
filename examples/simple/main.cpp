/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
/****************************************************************************/
#include <stream_transform/stream_transform.h>
#include <vector>


int main()
{
	namespace st = stream_transform;

	// Trivial example that uses the CPU-side implementation.
	std::vector<int> input({ 1,2,3,4,5,6,7,8 });
	std::vector<int> output(37,-1);
	st::SizeType outCount = 0;

	// This code is portable between GPU and CPU, assuming (of course) that the input, output & outCount are
	// accessible from the respective address space. Using this enables writing portable code and also to 
	// debug code on the CPU.
	st::transform(input, output, 
		[=]STFM_LAMBDA(int value, st::SizeType ind) { return value; },
		[=]STFM_LAMBDA(int value, st::SizeType ind, st::SizeType subInd)
	{ 
		return value * 1000 + subInd; 
	}, &outCount);

	// Same thing but using the index based interface, think of it as a nested for loop, where the second lambda is 
	// the body of the inner loop. Note that this version uses [&] to capture the reference to the std::vectors which 
	// is not something that translates too well to GPU, where you need some GPU friendly buffer type such as CudaDeviceBufferRef.
	// Also note that this interface does not check any output bounds, this is the responsibility of the user.
	st::transform(0, input.size(),
		[&]STFM_LAMBDA(st::SizeType ind) 
	{ 
		return input[ind];
	},
		[&]STFM_LAMBDA(st::SizeType inInd, st::SizeType outInd, st::SizeType subInd)
	{
		output[outInd] = input[inInd] * 1000 + subInd;
	}, &outCount);

	// Toy example that will not typically work on the GPU (the rand() functions the main problem), which
	// illustrates that we may make up completely the operations. However, note that under some implementations, such as
	// the two-pass implementation the count predicate may be called more than once and MUST thus be deterministic (which the below is not).
	st::transform(0, 64,
		[=]STFM_LAMBDA(st::SizeType ind)
	{
		// expansion ratio between 0 (compaction) and 7 output elements.
		return rand() % 8;
	},
		[=]STFM_LAMBDA(st::SizeType inInd, st::SizeType outInd, st::SizeType subInd)
	{
		printf("%d: %d/%d\n", outInd, inInd, subInd);
	}, nullptr);

	return 0;
}