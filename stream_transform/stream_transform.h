/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/****************************************************************************/
#ifndef _stream_transform_h_
#define _stream_transform_h_

#include "stream_transform_common.h"
#include "stream_transform_sp.h"
#include "stream_transform_tp.h"

#include <algorithm>
#include <type_traits>
#include <functional>


namespace stream_transform
{
#if !defined(__CUDACC__)

// Trivial, general, CPU fallback,
template <typename COUNT_PRED_T, typename EXP_PRED_T>
inline void transform(SizeType inStart, SizeType inEnd, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
{
	SizeType totalOut = 0;
	for (SizeType i = inStart; i < inEnd; ++i)
	{
		SizeType count = countPred(i);
		for (SizeType subInd = 0; subInd < count; ++subInd)
		{
			// NOTE: range check is not performed in the index based version, but is delegated to the user.
			// the iterator based implementation adds the range check in the first level lambda.
			expPred(i, totalOut, subInd);
			++totalOut;
		}
	}

	if (resultCount)
	{
		*resultCount = totalOut;
	}
}

// basic interface for unordered is different, requireing functions to get size from...
// this enables 'indirect' launch, where size is determined on the GPU-side.
template <typename IN_START_FN_T, typename IN_END_FN_T, typename COUNT_PRED_T, typename EXP_PRED_T, typename TT = typename std::enable_if<std::is_convertible<IN_START_FN_T, std::function<SizeType()>>::value>::type>
inline void transform_uo(IN_START_FN_T inStartFn, IN_END_FN_T inEndFn, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
{
	//static_assert(, "DSA");
	//static_assert(std::is_function<typename std::remove_pointer<typename IN_START_FN_T>::type>::value, "asd");
	transform(inStartFn(), inEndFn(), countPred, expPred, resultCount);
}

#endif // __CUDACC__



// Interface to ensure it can be called with size_t
template <typename IND1_T, typename IND2_T, typename COUNT_PRED_T, typename EXP_PRED_T, typename TT = typename std::enable_if<std::numeric_limits<IND1_T>::is_integer && std::numeric_limits<IND2_T>::is_integer>::type>
inline void transform(IND1_T inStart, IND2_T inEnd, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
{
	// 1. check for negative indexes
	assert(inStart >= 0);
	assert(inEnd >= 0);
	// 2. check that they don't overflow the max of SizeType
	assert(uint64_t(inStart) <= std::numeric_limits<SizeType>::max());
	assert(uint64_t(inEnd) <= std::numeric_limits<SizeType>::max());

	transform(SizeType(inStart), SizeType(inEnd), countPred, expPred, resultCount);
}


//
//// Interface to ensure it can be called with size_t
//template <typename IND1_T, typename IND2_T, typename COUNT_PRED_T, typename EXP_PRED_T, typename TT = std::enable_if<std::numeric_limits<IND1_T>::is_integer && std::numeric_limits<IND2_T>::is_integer>::type>
//inline void transform_uoX(IND1_T inStart, IND2_T inEnd, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
//{
//	// 1. check for negative indexes
//	assert(inStart >= 0);
//	assert(inEnd >= 0);
//	// 2. check that they don't overflow the max of SizeType
//	assert(uint64_t(inStart) <= std::numeric_limits<SizeType>::max());
//	assert(uint64_t(inEnd) <= std::numeric_limits<SizeType>::max());
//
//	transform_uo([=]STFM_LAMBDA() { return SizeType(inStart); }, [=]STFM_LAMBDA() { return SizeType(inEnd); }, countPred, expPred, resultCount);
//}



template <typename INPUT_RANGE_T, typename OUTPUT_RANGE_T, typename COUNT_PRED_T, typename EXP_PRED_T>
inline void transform(const INPUT_RANGE_T &inputRange, OUTPUT_RANGE_T &outputRange, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
{
	auto inBeg = inputRange.begin();
	auto outBeg = outputRange.begin();
	const size_t tmpInSize = inputRange.end() - inBeg;
	assert(tmpInSize <= std::numeric_limits<SizeType>::max());
	SizeType inputSize = SizeType(tmpInSize);

	const size_t tmpOutSize = outputRange.end() - outBeg;
	assert(tmpOutSize <= std::numeric_limits<SizeType>::max());
	SizeType outputSize = SizeType(tmpOutSize);

	// TODO: possibly move this logic to device-side for the GPU version since I'm not sure how the lambdas are handled when they are passed 
	// in from the outside like this. Will they get inlined properly?
	auto countPredInt = [=]STFM_LAMBDA(SizeType index)
	{
		return countPred(*(inBeg + index), index);
	};
	auto expPredInt = [=]STFM_LAMBDA(SizeType index, SizeType outIndex, SizeType subInd)->void
	{
		if (outIndex < outputSize)
		{
			*(outBeg + outIndex) = expPred(*(inBeg + index), index, subInd);
		}
	};

	transform(SizeType(0), inputSize, countPredInt, expPredInt, resultCount);
}

// r-value reference version to allow calling with a temporary - notably the views to mirrorbuffers...
template <typename INPUT_RANGE_T, typename OUTPUT_RANGE_T, typename COUNT_PRED_T, typename EXP_PRED_T, typename TT = typename std::enable_if<!std::numeric_limits<decltype(INPUT_RANGE_T())>::is_integer && !std::is_reference<OUTPUT_RANGE_T>::value >::type>
inline void transform(const INPUT_RANGE_T &inputRange, OUTPUT_RANGE_T &&outputRange, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
{
	//OUTPUT_RANGE_T tmpOr();
	transform<INPUT_RANGE_T, OUTPUT_RANGE_T, COUNT_PRED_T, EXP_PRED_T>(inputRange, outputRange, countPred, expPred, resultCount);
}

template <typename INPUT_RANGE_T, typename OUTPUT_RANGE_T, typename COUNT_PRED_T, typename EXP_PRED_T, typename TT = typename std::enable_if<!std::numeric_limits<decltype(INPUT_RANGE_T())>::is_integer>::type>
inline void transform_uo(const INPUT_RANGE_T &inputRange, OUTPUT_RANGE_T &outputRange, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
{
	auto inBeg = inputRange.begin();
	auto outBeg = outputRange.begin();

	// TODO: possibly move this logic to device-side for the GPU version since I'm not sure how the lambdas are handled when they are passed 
	// in from the outside like this. Will they get inlined properly?
	auto countPredInt = [=]STFM_LAMBDA(SizeType index)
	{
		return countPred(*(inBeg + index), index);
	};
	auto expPredInt = [=]STFM_LAMBDA(SizeType index, SizeType outIndex, SizeType subInd)->void
	{
		const size_t tmpOutSize = outputRange.end() - outBeg;
		assert(tmpOutSize <= std::numeric_limits<SizeType>::max());
		SizeType outputSize = SizeType(tmpOutSize);
		if (outIndex < outputSize)
		{
			*(outBeg + outIndex) = expPred(*(inBeg + index), index, subInd);
		}
	};

	transform_uo([=]STFM_LAMBDA()->SizeType { return SizeType(0); }, [=]STFM_LAMBDA()->SizeType
	{
		const size_t tmpInSize = inputRange.end() - inBeg;
		assert(tmpInSize <= std::numeric_limits<SizeType>::max());
		return SizeType(tmpInSize);
	}, countPredInt, expPredInt, resultCount);
}

// r-value reference version to allow calling with a temporary - notably the views to mirrorbuffers...
// NOTE: std::is_reference seems to be needed to disable the && overload on gcc 4.8.5 (possibly on others?)
template <typename INPUT_RANGE_T, typename OUTPUT_RANGE_T, typename COUNT_PRED_T, typename EXP_PRED_T, typename TT = typename std::enable_if<!std::numeric_limits<decltype(INPUT_RANGE_T())>::is_integer && !std::is_reference<OUTPUT_RANGE_T>::valu>::type>
inline void transform_uo(const INPUT_RANGE_T &inputRange, OUTPUT_RANGE_T &&outputRange, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
{
	//OUTPUT_RANGE_T tmpOr();
	transform_uo<INPUT_RANGE_T, OUTPUT_RANGE_T, COUNT_PRED_T, EXP_PRED_T>(inputRange, outputRange, countPred, expPred, resultCount);
}

/**
*/
//template <typename INPUT_RANGE_T, typename OUTPUT_RANGE_T, typename COUNT_PRED_T, typename EXP_PRED_T>
//void transform_uo(const INPUT_RANGE_T &inputRange, OUTPUT_RANGE_T &outputRange, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
//{
//	transform<INPUT_RANGE_T, OUTPUT_RANGE_T, COUNT_PRED_T, EXP_PRED_T>(inputRange, outputRange, countPred, expPred, resultCount);
//}



} // namespace stream_transform



#endif // _stream_transform_h_
