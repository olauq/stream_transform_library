/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/****************************************************************************/
#ifndef _stream_transform_sp_h_
#define _stream_transform_sp_h_

#include "stream_transform_common.h"

#ifdef __CUDACC__
#include <cub/warp/warp_reduce.cuh>
#include <cub/warp/warp_scan.cuh>
#include <cub/block/block_scan.cuh>
#include <cub/block/block_load.cuh>
#include <cub/block/block_store.cuh>

#define ST_SP_ENABLE_INDIRECT 0

/*
 * Single pass version
 */
namespace stream_transform
{

	namespace detail
{
#ifdef __CUDACC__

static constexpr uint32_t WARP_SIZE = 32;
static constexpr uint32_t g_singlePassInvalidCount = 0xFFFFFFFF;
static constexpr uint32_t g_singlePassChunkSumIncompleteFlag = 1U << 31U;
static constexpr uint32_t g_singlePassChunkSumPadding = WARP_SIZE;
static constexpr uint32_t g_numBlocks = 240;


template <typename V_T, typename PRED_T>
__device__ V_T reverse_warp_scan_step(const V_T v, PRED_T pred, int stepOffset, uint32_t laneInd)
{
	V_T r = cub::ShuffleDown<32>(v, stepOffset, 31, 0xffffffff);
	return pred(v, r, laneInd < WARP_SIZE - stepOffset);
}

/**
 * Computes a reverse prefix op, the boolean flag to the predicate can be used to exclude out-of-range accesses, if necessary
 * (in my use-case it isn't, so ignoring it speeds things up a little).
 */
template <typename V_T, typename PRED_T>
__device__ V_T reverse_warp_scan(V_T v, PRED_T pred, uint32_t laneInd)
{
	v = reverse_warp_scan_step(v, pred, 1, laneInd);
	v = reverse_warp_scan_step(v, pred, 2, laneInd);
	v = reverse_warp_scan_step(v, pred, 4, laneInd);
	v = reverse_warp_scan_step(v, pred, 8, laneInd);
	v = reverse_warp_scan_step(v, pred, 16, laneInd);
	return v;
}


class BufferManagerSp
{
public:
	uint32_t *m_chunkSumsBuffer;/**< Buffer */
	uint32_t m_chunkSumBufferSize;
	BufferManagerSp() : 
		m_chunkSumBufferSize(0), 
		m_chunkSumsBuffer(0)
	{
	}

	// Note: as of C++11 (at least), singletons like this one are thread safe and there will only ever be one.
	// However, some issues for a production system remain:
	//  1. it leaks the memory on exit, for the reason that it might otherwise try to deallocate the data after CUDA has been deinitialized.
	//  2. The rest is NOT thread safe, 
	//  3. Notably it is not even stream safe (as in two CUDA streams may try to use the same buffer, even using only one CPU thread).
	static BufferManagerSp& instance()
	{
		static BufferManagerSp inst;
		return inst;
	}

	// TODO: Make this re-entrant
	uint32_t *getChunkSumBuffer(uint32_t size)
	{
		if (m_chunkSumBufferSize < size)
		{
			if (m_chunkSumsBuffer)
			{
				cudaFree(m_chunkSumsBuffer);
				m_chunkSumsBuffer = 0;
			}
			cudaMalloc(&m_chunkSumsBuffer, sizeof(uint32_t) * size);
			m_chunkSumBufferSize = size;
		}
		return m_chunkSumsBuffer;
	}

private:
	// No copying please!
	BufferManagerSp(const BufferManagerSp&) { }
};


__global__ void prepChunksKernel(uint32_t * chunkSums, uint32_t numChunks)
{
	const int i = blockIdx.x * blockDim.x + threadIdx.x;
	if (i < numChunks + g_singlePassChunkSumPadding)
	{
		chunkSums[i] = i < g_singlePassChunkSumPadding ? 0U : g_singlePassInvalidCount;
	}
}



template <uint32_t NUM_WARPS, uint32_t ITEMS_PER_THREAD, typename COUNT_PRED_T, typename EXP_FN_T>
__global__ void transformSinglePassIndsKernel(SizeType inStart, SizeType inEnd, uint32_t * chunkSums, COUNT_PRED_T countPred, EXP_FN_T expPred, SizeType *resultCount)
{
	using namespace detail;

	const uint32_t length = inEnd - inStart;

	const uint32_t chunkIndex = blockIdx.x;
	const uint32_t chunkSize = NUM_WARPS * WARP_SIZE * ITEMS_PER_THREAD;
	const uint32_t numChunks = (length + chunkSize - 1U) / chunkSize;
	// 1. load the data, and pack X per thread into registers for extra fun and profit...
	uint32_t chunkStartIndex = chunkIndex * chunkSize;

	const uint32_t laneInd = threadIdx.x;
	const uint32_t warpInd = threadIdx.y;
	const uint32_t laneMask = ((1U << (laneInd + 1U)) - 1);
	//if (chunkIndex == 0)
	//printf("Q[%d]: laneMask %x\n", laneInd, laneMask);

	// TODOL cache values!
	//T values[ITEMS_PER_THREAD];
	uint32_t counts[ITEMS_PER_THREAD];
	const uint32_t numItems = min(length - chunkStartIndex, chunkSize);
	uint32_t threadOutputCount = 0;

	const uint32_t warpInOffset = WARP_SIZE * ITEMS_PER_THREAD * warpInd;
#pragma unroll
	for (uint32_t i = 0; i < ITEMS_PER_THREAD; ++i)
	{
		const uint32_t inOffset = warpInOffset + WARP_SIZE * i + laneInd;
		bool isValid = inOffset < numItems;
		//		if (isValid)
		//	{
		//	values[i] = inData[chunkStartIndex + inOffset];
		//printf("%d\n", chunkStartIndex + inOffset);
		//		}
		counts[i] = isValid ? countPred(chunkStartIndex + inOffset) : 0U;
		threadOutputCount += counts[i];
	}

	// 2. calculate the total output count.
	typedef cub::WarpReduce<uint32_t> WarpReduce;
	typedef cub::WarpScan<uint32_t> WarpScan;
	using  WarpScanWarpSum = cub::WarpScan<uint32_t, NUM_WARPS>;
	union Shared
	{
		WarpReduce::TempStorage reduceTmp;
		WarpScan::TempStorage scanTmp;
		typename WarpScanWarpSum::TempStorage scanTmpWs;
		volatile uint32_t srcLaneIndexBuffer[WARP_SIZE];
	};
	__shared__ Shared sh[NUM_WARPS];
	__shared__ uint32_t sh_warpOffsets[NUM_WARPS];
	__shared__ uint32_t sh_blockOffset;
	//__shared__ uint32_t sh_blockCount;

	uint32_t warpTotal = WarpReduce(sh[warpInd].reduceTmp).Sum(threadOutputCount);
	//printf("%d.%d.%d:  %d\n", chunkIndex, warpInd, laneInd, warpTotal);
	if (laneInd == 0)
	{
		sh_warpOffsets[warpInd] = warpTotal;
	}
	__syncthreads();
	// let first warp work out warp offsets
	if (warpInd == 0)
	{
		uint32_t blockOutputCount = 0;
		uint32_t v = laneInd < NUM_WARPS ? sh_warpOffsets[laneInd] : 0U;

		//if (laneInd < NUM_WARPS && chunkIndex == 0)
		//{
		//	printf("%d, ", v);
		//}
		//if (laneInd == 0 && chunkIndex == 0)
		//{
		//	printf("\n");
		//}

		uint32_t warpOffset = 0;
		WarpScanWarpSum(sh[warpInd].scanTmpWs).ExclusiveSum(v, warpOffset, blockOutputCount);
		// store back the offset.
		if (laneInd < NUM_WARPS)
		{
			sh_warpOffsets[laneInd] = warpOffset;
		}


		// We now have the block total, so time to update the global thingo
		// First chunk, no waiting.

		// First, publish the block sum for all to see, if this is not the first thread, then flag as incomplete
		if (laneInd == 0) // first lane of first warp
		{
			cub::ThreadStore<cub::STORE_CG>(chunkSums + chunkIndex + g_singlePassChunkSumPadding, chunkIndex == 0 ? blockOutputCount : (blockOutputCount | g_singlePassChunkSumIncompleteFlag));
		}

		// First chunk, not much to do
		if (chunkIndex == 0)
		{
			sh_blockOffset = 0U;
			//sh_blockCount = blockOutputCount;
		}
		else
		{
			// 1. wait for previous chunks to become at least incomplete, then we can work our way back
			uint32_t prevIndex = chunkIndex + laneInd;// + g_singlePassChunkSumPadding - WARP_SIZE;

			uint32_t completeMask = 0;
			uint32_t offset = 0;
			do
			{
				//printf("%d.%d.%d:  %d\n", chunkIndex, warpInd, laneInd, prevIndex);
				assert(int(prevIndex) >= 0);
				assert(prevIndex < g_singlePassChunkSumPadding + (length + chunkSize - 1) / chunkSize);
				uint32_t prevOffset = cub::ThreadLoad<cub::LOAD_CG>(chunkSums + prevIndex);

				// repeat until all are valid
				while (prevOffset == g_singlePassInvalidCount)
				{
					__threadfence_block(); // prevent hoisting loads from loop
					prevOffset = cub::ThreadLoad<cub::LOAD_CG>(chunkSums + prevIndex);
				}
				// Now we have 32 previous values all of which are either incomplete or 
				//printf("%d.%d.%d: prevOffset %d\n", chunkIndex, warpInd, laneInd, prevOffset);

				// last bit set is the last completed sum
				completeMask = __ballot_sync(0xffffffff, (prevOffset & g_singlePassChunkSumIncompleteFlag) == 0);

				//if (laneInd == 0)
				//	printf("%d.%d.%d:  completeMask %x\n", chunkIndex, warpInd, laneInd, completeMask);

				uint32_t v = laneMask >= completeMask ? (prevOffset & ~g_singlePassChunkSumIncompleteFlag) : 0U;

				offset += WarpReduce(sh[warpInd].reduceTmp).Sum(v);

				// shift window one warp down, since we padded the chunk sum buffer with zeroes, this is safe to do.
				prevIndex -= WARP_SIZE;
				// Exit when one is complete.
			} while (completeMask == 0);


			if (laneInd == 0) // first lane of first warp
			{
				// 2. update the final total for current chunk
				cub::ThreadStore<cub::STORE_CG>(chunkSums + chunkIndex + g_singlePassChunkSumPadding, offset + blockOutputCount);
				//printf("chunk %4d: %d\n", chunkIndex, offset + blockOutputCount);
				// 3. broadcast offset to rest of block
				sh_blockOffset = offset;
				//sh_blockCount = blockOutputCount;
			}
		}
		if (laneInd == 0 && resultCount && chunkIndex == numChunks - 1U)
		{
			*resultCount = sh_blockOffset + blockOutputCount;
		}
	}
	__syncthreads();
#if 1

	// 4. perform transform, warps are now on their own

	// early out, note that only SIMD lane 0 actually has a valid value for this, so this is not the best design really...
	if (__all_sync(0xffffffff, warpTotal == 0))
	{
		return;
	}

	uint32_t outOffset = sh_warpOffsets[warpInd] + sh_blockOffset;


	const uint32_t invalidValue = 0xFFFFFFFF;
#pragma unroll
	for (uint32_t i = 0; i < ITEMS_PER_THREAD; ++i)
	{
		uint32_t baseInIndex = chunkStartIndex + warpInOffset + WARP_SIZE * i;
		//if (laneInd == 0)
		//{
		//	printf("X %d\n", baseInIndex);
		//}

		//bool isValid = (i * BLOCK_SIZE) + threadIdx.x < numItems;
		uint32_t count = counts[i];
		// 4. generate output items 
		uint32_t offset;
		uint32_t totalOutputCount;
		WarpScan(sh[warpInd].scanTmp).ExclusiveSum(count, offset, totalOutputCount);
		//if (chunkIndex == 1)
		//	printf("Q[%d]: %d\n", laneInd, totalOutputCount);

		int outIntervalStart = int(offset);
		// Setting the output interval to negative simplifies the test in the loop
		int outIntervalStop = count ? outIntervalStart + int(count) - 1 : -1;

		for (uint32_t outIndex = laneInd; outIndex < totalOutputCount + laneInd; outIndex += WARP_SIZE)
		{
			// prep buffer with invalid values...
			sh[warpInd].srcLaneIndexBuffer[laneInd] = invalidValue;

			//if (chunkIndex == 1)
			//	printf("A[%d]: %d %d\n", laneInd, outIntervalStart, outIntervalStop);
			// Write the (input) lane index to the end of the interval in the shared buffer
			if (outIntervalStop >= 0 && outIntervalStart < int(WARP_SIZE))
			{
				// clamp if crossing the boundary
				sh[warpInd].srcLaneIndexBuffer[min(int(WARP_SIZE) - 1, outIntervalStop)] = laneInd;
				//if (chunkIndex == 1)
				//	printf("slib[%d] = %d\n", min(int(WARP_SIZE) - 1, outIntervalStop), laneInd);
			}

			// reverse prefix computes the input index for each SIMD lane
			int srcLaneIndex = reverse_warp_scan(sh[warpInd].srcLaneIndexBuffer[laneInd],
				[=](uint32_t v, uint32_t r, bool inRange)
			{
				if (v == invalidValue)
				{
					return r;
				}
				return v;
			}, laneInd);
			//if (chunkIndex == 1)
			//	printf("Y[%d]: %d %d %d %d\n", laneInd, srcLaneIndex, length, outIntervalStart, outIntervalStop);
			uint32_t subIndex = outIndex - __shfl_sync(0xffffffff, offset, srcLaneIndex);
			uint32_t srcIndex = baseInIndex + srcLaneIndex;

			if (outIndex < totalOutputCount)
			{
				assert(srcIndex < length);

				// TODO: use cub::Shuffle to get the value from the source lane instead
				expPred(srcIndex, outOffset + outIndex, subIndex);
			}

			outIntervalStart -= int(WARP_SIZE);
			outIntervalStop -= int(WARP_SIZE);
		}

		outOffset += totalOutputCount;
	}
#endif
}



template <uint32_t NUM_WARPS, uint32_t ITEMS_PER_THREAD, typename INPUT_IT_T, typename OUTPUT_IT_T, typename COUNT_PRED_T, typename EXP_FN_T>
__global__ void transformSinglePassKernel(INPUT_IT_T inData, uint32_t length, OUTPUT_IT_T outData, uint32_t * chunkSums, COUNT_PRED_T countPred, EXP_FN_T expPred, SizeType *resultCount)
{
	using namespace detail;

	const uint32_t chunkIndex = blockIdx.x;
	const uint32_t chunkSize = NUM_WARPS * WARP_SIZE * ITEMS_PER_THREAD;
	const uint32_t numChunks = (length + chunkSize - 1U) / chunkSize;
	// 1. load the data, and pack X per thread into registers for extra fun and profit...
	uint32_t chunkStartIndex = chunkIndex * chunkSize;

	const uint32_t laneInd = threadIdx.x;
	const uint32_t warpInd = threadIdx.y;
	const uint32_t laneMask = ((1U << (laneInd + 1U)) - 1);
	//if (chunkIndex == 0)
	//printf("Q[%d]: laneMask %x\n", laneInd, laneMask);

	// TODOL cache values!
	//T values[ITEMS_PER_THREAD];
	uint32_t counts[ITEMS_PER_THREAD];
	const uint32_t numItems = min(length - chunkStartIndex, chunkSize);
	uint32_t threadOutputCount = 0;

	const uint32_t warpInOffset = WARP_SIZE * ITEMS_PER_THREAD * warpInd;
#pragma unroll
	for (uint32_t i = 0; i < ITEMS_PER_THREAD; ++i)
	{
		const uint32_t inOffset = warpInOffset + WARP_SIZE * i + laneInd;
		bool isValid = inOffset < numItems;
//		if (isValid)
	//	{
		//	values[i] = inData[chunkStartIndex + inOffset];
			//printf("%d\n", chunkStartIndex + inOffset);
//		}
		counts[i] = isValid ? countPred(*(inData + chunkStartIndex + inOffset)) : 0U;
		threadOutputCount += counts[i];
	}

	// 2. calculate the total output count.
	typedef cub::WarpReduce<uint32_t> WarpReduce;
	typedef cub::WarpScan<uint32_t> WarpScan;
	using  WarpScanWarpSum = cub::WarpScan<uint32_t, NUM_WARPS>;
	union Shared
	{
		WarpReduce::TempStorage reduceTmp;
		WarpScan::TempStorage scanTmp;
		typename WarpScanWarpSum::TempStorage scanTmpWs;
		volatile uint32_t srcLaneIndexBuffer[WARP_SIZE];
	};
	__shared__ Shared sh[NUM_WARPS];
	__shared__ uint32_t sh_warpOffsets[NUM_WARPS];
	__shared__ uint32_t sh_blockOffset;
	//__shared__ uint32_t sh_blockCount;

	uint32_t warpTotal = WarpReduce(sh[warpInd].reduceTmp).Sum(threadOutputCount);
	//printf("%d.%d.%d:  %d\n", chunkIndex, warpInd, laneInd, warpTotal);
	if (laneInd == 0)
	{
		sh_warpOffsets[warpInd] = warpTotal;
	}
	__syncthreads();
	// let first warp work out warp offsets
	if (warpInd == 0)
	{
		uint32_t blockOutputCount = 0;
		uint32_t v = laneInd < NUM_WARPS ? sh_warpOffsets[laneInd] : 0U;

		//if (laneInd < NUM_WARPS && chunkIndex == 0)
		//{
		//	printf("%d, ", v);
		//}
		//if (laneInd == 0 && chunkIndex == 0)
		//{
		//	printf("\n");
		//}

		uint32_t warpOffset = 0;
		WarpScanWarpSum(sh[warpInd].scanTmpWs).ExclusiveSum(v, warpOffset, blockOutputCount);
		// store back the offset.
		if (laneInd < NUM_WARPS)
		{
			sh_warpOffsets[laneInd] = warpOffset;
		}


		// We now have the block total, so time to update the global thingo
		// First chunk, no waiting.

		// First, publish the block sum for all to see, if this is not the first thread, then flag as incomplete
		if (laneInd == 0) // first lane of first warp
		{
			cub::ThreadStore<cub::STORE_CG>(chunkSums + chunkIndex + g_singlePassChunkSumPadding, chunkIndex == 0 ? blockOutputCount : (blockOutputCount | g_singlePassChunkSumIncompleteFlag));
		}

		// First chunk, not much to do
		if (chunkIndex == 0)
		{
			sh_blockOffset = 0U;
			//sh_blockCount = blockOutputCount;
		}
		else
		{
			// 1. wait for previous chunks to become at least incomplete, then we can work our way back
			uint32_t prevIndex = chunkIndex + laneInd;// + g_singlePassChunkSumPadding - WARP_SIZE;

			uint32_t completeMask = 0;
			uint32_t offset = 0;
			do
			{
				//printf("%d.%d.%d:  %d\n", chunkIndex, warpInd, laneInd, prevIndex);
				assert(int(prevIndex) >= 0);
				assert(prevIndex < g_singlePassChunkSumPadding + (length + chunkSize - 1) / chunkSize);
				uint32_t prevOffset = cub::ThreadLoad<cub::LOAD_CG>(chunkSums + prevIndex);

				// repeat until all are valid
				while (prevOffset == g_singlePassInvalidCount)
				{
					__threadfence_block(); // prevent hoisting loads from loop
					prevOffset = cub::ThreadLoad<cub::LOAD_CG>(chunkSums + prevIndex);
				}
				// Now we have 32 previous values all of which are either incomplete or 
				//printf("%d.%d.%d: prevOffset %d\n", chunkIndex, warpInd, laneInd, prevOffset);

				// last bit set is the last completed sum
				completeMask = __ballot_sync(0xffffffff, (prevOffset & g_singlePassChunkSumIncompleteFlag) == 0);

				//if (laneInd == 0)
				//	printf("%d.%d.%d:  completeMask %x\n", chunkIndex, warpInd, laneInd, completeMask);

				uint32_t v = laneMask >= completeMask ? (prevOffset & ~g_singlePassChunkSumIncompleteFlag) : 0U;

				offset += WarpReduce(sh[warpInd].reduceTmp).Sum(v);

				// shift window one warp down, since we padded the chunk sum buffer with zeroes, this is safe to do.
				prevIndex -= WARP_SIZE;
				// Exit when one is complete.
			} while (completeMask == 0);


			if (laneInd == 0) // first lane of first warp
			{
				// 2. update the final total for current chunk
				cub::ThreadStore<cub::STORE_CG>(chunkSums + chunkIndex + g_singlePassChunkSumPadding, offset + blockOutputCount);
				//printf("chunk %4d: %d\n", chunkIndex, offset + blockOutputCount);
				// 3. broadcast offset to rest of block
				sh_blockOffset = offset;
				//sh_blockCount = blockOutputCount;
			}
		}
		if (laneInd == 0 && resultCount && chunkIndex == numChunks - 1U)
		{
			*resultCount = sh_blockOffset + blockOutputCount;
		}
	}
	__syncthreads();
#if 1

	// 4. perform transform, warps are now on their own

	// early out, note that only SIMD lane 0 actually has a valid value for this, so this is not the best design really...
	if (__all_sync(0xffffffff, warpTotal == 0))
	{
		return;
	}

	uint32_t outOffset = sh_warpOffsets[warpInd] + sh_blockOffset;


	const uint32_t invalidValue = 0xFFFFFFFF;
#pragma unroll
	for (uint32_t i = 0; i < ITEMS_PER_THREAD; ++i)
	{
		uint32_t baseInIndex = chunkStartIndex + warpInOffset + WARP_SIZE * i;
		//if (laneInd == 0)
		//{
		//	printf("X %d\n", baseInIndex);
		//}

		//bool isValid = (i * BLOCK_SIZE) + threadIdx.x < numItems;
		uint32_t count = counts[i];
		// 4. generate output items 
		uint32_t offset;
		uint32_t totalOutputCount;
		WarpScan(sh[warpInd].scanTmp).ExclusiveSum(count, offset, totalOutputCount);
		//if (chunkIndex == 1)
		//	printf("Q[%d]: %d\n", laneInd, totalOutputCount);

		int outIntervalStart = int(offset);
		// Setting the output interval to negative simplifies the test in the loop
		int outIntervalStop = count ? outIntervalStart + int(count) - 1 : -1;

		for (uint32_t outIndex = laneInd; outIndex < totalOutputCount + laneInd; outIndex += WARP_SIZE)
		{
			// prep buffer with invalid values...
			sh[warpInd].srcLaneIndexBuffer[laneInd] = invalidValue;

			//if (chunkIndex == 1)
			//	printf("A[%d]: %d %d\n", laneInd, outIntervalStart, outIntervalStop);
			// Write the (input) lane index to the end of the interval in the shared buffer
			if (outIntervalStop >= 0 && outIntervalStart < int(WARP_SIZE))
			{
				// clamp if crossing the boundary
				sh[warpInd].srcLaneIndexBuffer[min(int(WARP_SIZE) - 1, outIntervalStop)] = laneInd;
				//if (chunkIndex == 1)
				//	printf("slib[%d] = %d\n", min(int(WARP_SIZE) - 1, outIntervalStop), laneInd);
			}

			// reverse prefix computes the input index for each SIMD lane
			int srcLaneIndex = reverse_warp_scan(sh[warpInd].srcLaneIndexBuffer[laneInd],
				[=](uint32_t v, uint32_t r, bool inRange)
			{
				if (v == invalidValue)
				{
					return r;
				}
				return v;
			}, laneInd);
			//if (chunkIndex == 1)
			//	printf("Y[%d]: %d %d %d %d\n", laneInd, srcLaneIndex, length, outIntervalStart, outIntervalStop);
			uint32_t subIndex = outIndex - __shfl_sync(0xffffffff, offset, srcLaneIndex);
			uint32_t srcIndex = baseInIndex + srcLaneIndex;

			if (outIndex < totalOutputCount)
			{
				assert(srcIndex < length);

				// TODO: use cub::Shuffle to get the value from the source lane instead
				*(outData + outOffset + outIndex) = expPred(*(inData + srcIndex), srcIndex, subIndex);
			}

			outIntervalStart -= int(WARP_SIZE);
			outIntervalStop -= int(WARP_SIZE);
		}

		outOffset += totalOutputCount;
	}
#endif
}

#endif // __CUDACC__


} // namespace detail



template <typename COUNT_PRED_T, typename EXP_PRED_T>
void transform(SizeType inStart, SizeType inEnd, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
{
	// TODO: add guard against re-entry, set a flag or something, for debug purposes.
#ifdef __CUDACC__
	using namespace detail;

	static const int warpsPerBlock = 8;
	static const int itemsPerThread = 8;
	const dim3 blockSize(WARP_SIZE, warpsPerBlock);
	static const uint32_t chunkSize = warpsPerBlock * WARP_SIZE * itemsPerThread;

#if ST_SP_ENABLE_INDIRECT
	const uint32_t maxChunks = (inputRange.capacity() + chunkSize - 1U) / chunkSize;
	uint32_t *chunkSums = BufferManagerSp::instance().getChunkSumBuffer(maxChunks + g_singlePassChunkSumPadding);
	prepChunksKernel<<<(maxChunks + g_singlePassChunkSumPadding + 128 - 1) / 128, 128>>>(chunkSums, maxChunks);
	transformSinglePassIndsKernel<warpsPerBlock, itemsPerThread><<<g_numBlocks, blockSize>>>(inputRange, outIt, chunkSums, countPred, expPred, resultCount);
#else // !ST_SP_ENABLE_INDIRECT
	// Note, this must be done in the kernel as we support indirect range size for both input and output ranges
	//       they may thus fetch the size from a device side variable.
	const uint32_t length = inEnd - inStart;

	const uint32_t numChunks = (length + chunkSize - 1U) / chunkSize;

	uint32_t *chunkSums = BufferManagerSp::instance().getChunkSumBuffer(numChunks + g_singlePassChunkSumPadding);

	prepChunksKernel<<<(numChunks + g_singlePassChunkSumPadding + 128 - 1) / 128, 128>>>(chunkSums, numChunks);
	//CUDA_CHECK_ERROR("gen_transform");

	transformSinglePassIndsKernel<warpsPerBlock, itemsPerThread><<<numChunks, blockSize>>>(inStart, inEnd, chunkSums, countPred, expPred, resultCount);
#endif // ST_SP_ENABLE_INDIRECT

#else // ! defined __CUDACC__

	SizeType totalOut = 0;
	for (SizeType i = inStart; i < inEnd; ++i)
	{
		SizeType count = countPred(i);
		for (SizeType subInd = 0; subInd < count; ++subInd)
		{
			// NOTE: range check is not performed in the index based version, but is delegated to the user.
			// the iterator based implementation adds the range check in the first level lambda.
			expPred(i, totalOut, subInd);
			++totalOut;
		}
	}

	if (resultCount)
	{
		*resultCount = totalOut;
	}

#endif // __CUDACC__
}

}; // namespace stream_transform

#endif // __CUDACC__

#endif // _stream_transform_sp_h_
