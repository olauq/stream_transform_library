/****************************************************************************/
/* Copyright (c) 2016-2018, Ola Olsson
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORTILLA OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/
/****************************************************************************/

#ifndef _WarpDispatcher_cuh_
#define _WarpDispatcher_cuh_


/** 
 * This library provides very basic support of the programming model presented in "Efficient Stream Compaction on wide SIMD many-core architectures".
 * Implements support for dispatching warp-sychronous work to the GPU (so S from that model is fixed at 32 == g_warpSize). 
 * Helps with dividing up the work into warp sized chunks and allocates a range to each warp.
 *
 *  TODO: 
 *   - make uniform support of indirect launches, add functor to all flavours. Also add pointer interface?
 *   - support launch configuration, at least compile time selection of warps/block and total warps. For example, make use of Mark Harris Hemi library to perform launch configs.
 */
namespace WarpDispatcher
{

namespace detail
{


struct EmptyShared
{
};
}

enum 
{ 
	g_warpSize = 32,
	g_numWarpsPerBlock = 8, //1,//4,//6,
	// NOTE: some things really dont produce a very balanced wrorkload, so it's pretty high...
	g_numBlocks = 240,//1, // 2048, //512, //1024,//30, //120, // 90, // 150, // 240, //a fixed number of blocks in order to load balance means we will need > g_numBlocks * g_numWarpsPerBlock * g_warpChunkSize to be efficient, or 4 * 128 * 1024 = 
  g_numProcessors = g_numWarpsPerBlock * g_numBlocks,

};


inline __device__ int globalWarpId()
{
	return threadIdx.y + blockIdx.x * g_numWarpsPerBlock;
}

inline __device__ int localWarpId()
{
	return threadIdx.y;
}

inline __device__ int laneId()
{
	return threadIdx.x;
}

inline __device__ int localThreadId()
{
	return threadIdx.x + threadIdx.y * g_warpSize;
}

struct ChunkConfig
{
	uint32_t numChunks;
	uint32_t baseChunks;
	uint32_t restChunks;

	__device__ __host__ ChunkConfig(uint32_t length)
	{
		numChunks = (length + g_warpSize - 1) / g_warpSize;
		baseChunks = numChunks / g_numProcessors;
		restChunks = numChunks % g_numProcessors;
	}

	__device__ __host__ uint32_t chunkRangeStart(uint32_t processorIndex)
	{
		return baseChunks * processorIndex + min(processorIndex, restChunks);
	}

	__device__ __host__ uint32_t chunkRangeSize(uint32_t processorIndex)
	{
		return baseChunks + (processorIndex < restChunks);
	}

	__device__ __host__ uint32_t chunkRangeEnd(uint32_t processorIndex)
	{
		return chunkRangeStart(processorIndex) + chunkRangeSize(processorIndex);
	}

	__device__ __host__ uint32_t chunkRangeSizeBound()
	{
		return baseChunks + restChunks > 0 ? 1 : 0;
	}
		
};

template <typename KERNEL_FUNCTOR_T>
__global__ void dispatcherDriverKernel(KERNEL_FUNCTOR_T kernel, uint32_t length)
{
	const uint32_t processorId = globalWarpId();
	//const uint32_t laneId = laneId();
	const uint32_t numChunks = (length + g_warpSize - 1) / g_warpSize;
	const uint32_t baseNumIterations = numChunks / g_numProcessors;
	const uint32_t restChunks = numChunks % g_numProcessors;
	const uint32_t rangeStart = baseNumIterations * processorId + min(processorId, restChunks);
  const uint32_t rangeEnd = rangeStart + baseNumIterations + (processorId < restChunks);

	if (laneId() == 0)
	{
		//printf("%d %d %d %d %d %d\n", processorId, length, baseNumIterations, restChunks, rangeStart, rangeEnd);
	}

	//__shared__ typename KERNEL_FUNCTOR_T::Shared shared[g_numWarpsPerBlock];

	//kernel(rangeStart, min(rangeEnd, length), laneId);// , shared[threadIdx.y]);

	for (int wi = rangeStart * g_warpSize; wi < rangeEnd * g_warpSize; wi += g_warpSize)
	{
		int i = wi + laneId();
		kernel(i, i < length);
	}
}



template <typename SHARED_T, typename KERNEL_FUNCTOR_T>
__global__ void dispatcherDriverKernel(KERNEL_FUNCTOR_T kernel, uint32_t length)
{
	const uint32_t processorId = globalWarpId();
	//const uint32_t laneId = laneId();
	const uint32_t numChunks = (length + g_warpSize - 1) / g_warpSize;
	const uint32_t baseNumIterations = numChunks / g_numProcessors;
	const uint32_t restChunks = numChunks % g_numProcessors;
	const uint32_t rangeStart = baseNumIterations * processorId + min(processorId, restChunks);
  const uint32_t rangeEnd = rangeStart + baseNumIterations + (processorId < restChunks);

	if (laneId() == 0)
	{
		//printf("%d %d %d %d %d %d\n", processorId, length, baseNumIterations, restChunks, rangeStart, rangeEnd);
	}

	__shared__ SHARED_T shared[g_numWarpsPerBlock];

	for (int wi = rangeStart * g_warpSize; wi < rangeEnd * g_warpSize; wi += g_warpSize)
	{
		int i = wi + laneId();
		kernel(i, i < length, shared[localWarpId()]);
	}
}



template <typename SHARED_T, typename KERNEL_FUNCTOR_T>
void dispatchSimdShared(uint32_t length, KERNEL_FUNCTOR_T kernel)
{
	// TODO: work out how much shared mem is avaliable, given register use etc, and just allocate that.
	//       look out for bankin, alignment, etc which could obstruct...
	// TODO: launch optimal number of processors, using occupancy and GPU caps.
	dim3 blockDim(g_warpSize, g_numWarpsPerBlock);
	dispatcherDriverKernel<SHARED_T><<<g_numBlocks, blockDim >>>(kernel, length);
}



template <typename KERNEL_FUNCTOR_T>
void dispatchSimd(uint32_t length, KERNEL_FUNCTOR_T kernel)
{
	dispatchSimdShared<detail::EmptyShared>(length, [=] __device__(uint32_t v, bool isActive, detail::EmptyShared &) { kernel(v, isActive); });
}



template <typename SHARED_T, typename KERNEL_FUNCTOR_T>
__global__ void dispatcherDriverRangeKernel(KERNEL_FUNCTOR_T kernel, uint32_t length)
{
	const uint32_t processorId = globalWarpId();
	//const uint32_t laneId = laneId();
	const uint32_t numChunks = (length + g_warpSize - 1) / g_warpSize;
	const uint32_t baseNumIterations = numChunks / g_numProcessors;
	const uint32_t restChunks = numChunks % g_numProcessors;
	const uint32_t rangeStart = baseNumIterations * processorId + min(processorId, restChunks);
	const uint32_t rangeEnd = rangeStart + baseNumIterations + (processorId < restChunks);

	__shared__ SHARED_T shared[g_numWarpsPerBlock];
	kernel(rangeStart * g_warpSize + laneId(), min(rangeEnd * g_warpSize, length), shared[localWarpId()]);
}

template <typename SHARED_T, typename KERNEL_FUNCTOR_T>
void dispatchRangeShared(uint32_t length, KERNEL_FUNCTOR_T kernel)
{
	// TODO: work out how much shared mem is avaliable, given register use etc, and just allocate that.
	//       look out for bankin, alignment, etc which could obstruct...
	// TODO: launch optimal number of processors, using occupancy and GPU caps.
	dim3 blockDim(g_warpSize, g_numWarpsPerBlock);
	dispatcherDriverRangeKernel<SHARED_T> <<<g_numBlocks, blockDim >>>(kernel, length);
}



template <typename SHARED_T, typename KERNEL_FUNCTOR_T, typename LENGTH_FUNC_T>
__global__ void dispatcherDriverChunksRangeKernel(KERNEL_FUNCTOR_T kernel, LENGTH_FUNC_T lengthFn)
{
	const uint32_t length = lengthFn();

	const uint32_t processorId = globalWarpId();
	const uint32_t numChunks = (length + g_warpSize - 1) / g_warpSize;
	const uint32_t baseNumIterations = numChunks / g_numProcessors;
	const uint32_t restChunks = numChunks % g_numProcessors;
	const uint32_t rangeStart = baseNumIterations * processorId + min(processorId, restChunks);
	const uint32_t rangeEnd = rangeStart + baseNumIterations + (processorId < restChunks);

	__shared__ SHARED_T shared[g_numWarpsPerBlock];
	kernel(rangeStart, rangeEnd, shared[localWarpId()]);
}

/**
 * Dispatch a kernel with arguments (uint32_t chunkRangeStart, uint32_t chunkRangeEnd, SHARED_T &sh) 
 * it will be called once for each global warp, with the chunk range as arguments. To get the element
 * range, this value is multiplied by g_warpSize. If the input length is not a multiple of g_warpSize
 * care must be taken by the kernel to not access memory out of range.
 */
template <typename SHARED_T, typename KERNEL_FUNCTOR_T>
void dispatchChunksRangeShared(uint32_t length, KERNEL_FUNCTOR_T kernel)
{
	dim3 blockDim(g_warpSize, g_numWarpsPerBlock);
	dispatcherDriverChunksRangeKernel<SHARED_T> <<<g_numBlocks, blockDim >>>(kernel, [=]__device__() { return length; });
}

template <typename SHARED_T, typename LENGTH_FUNC_T, typename KERNEL_FUNCTOR_T>
void dispatchChunksRangeShared(LENGTH_FUNC_T lengthFn, KERNEL_FUNCTOR_T kernel)
{
	dim3 blockDim(g_warpSize, g_numWarpsPerBlock);
	dispatcherDriverChunksRangeKernel<SHARED_T> <<<g_numBlocks, blockDim >>>(kernel, lengthFn);
}


} // WarpDispatcher


#endif // #ifndef _WarpDispatcher_cuh_
