/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/****************************************************************************/
#ifndef _stream_transform_tp_h_
#define _stream_transform_tp_h_

#include "stream_transform_common.h"

#ifdef __CUDACC__

#include "WarpDispatcher.cuh"
#include <cub/warp/warp_reduce.cuh>
#include <cub/warp/warp_scan.cuh>
#include <cub/block/block_scan.cuh>
#include <cub/block/block_load.cuh>
#include <cub/block/block_store.cuh>


namespace stream_transform
{

namespace wd = WarpDispatcher;

namespace detail
{

template<unsigned N>
struct NextPowerOf2
{
private:
	enum { t1 = N-1 };
	enum { t2 = t1 | (t1 >> 1) };
	enum { t3 = t2 | (t2 >> 2) };
	enum { t4 = t3 | (t3 >> 4) };
	enum { t5 = t4 | (t4 >> 8) };
	enum { t6 = t5 | (t5 >> 16) };

public:
	enum { value = t6 + 1 };
};

// step in the custom reverse prefix implementation
template <typename V_T, typename PRED_T>
STFM_CUDA_FN_D V_T reverse_warp_scan_step(const V_T v, PRED_T pred, int stepOffset)
{
	V_T r = cub::ShuffleDown<32>(v, stepOffset, 31, 0xffffffff);
	return pred(v, r, wd::laneId() < wd::g_warpSize - stepOffset);
}

/**
* Computes a reverse prefix op, the boolean flag to the predicate can be used to exclude out-of-range accesses, if necessary
* (in my use-case it isn't, so ignoring it speeds things up a little).
*/
template <typename V_T, typename PRED_T>
STFM_CUDA_FN_D V_T reverse_warp_scan(V_T v, PRED_T pred)
{
	v = reverse_warp_scan_step(v, pred, 1);
	v = reverse_warp_scan_step(v, pred, 2);
	v = reverse_warp_scan_step(v, pred, 4);
	v = reverse_warp_scan_step(v, pred, 8);
	v = reverse_warp_scan_step(v, pred, 16);
	return v;
}


enum
{
	g_prefixBufferSize = NextPowerOf2<wd::g_numProcessors>::value >= 1024 ? NextPowerOf2<wd::g_numProcessors>::value : 1024
};


struct TransformShared
{
	typename WarpScan::TempStorage cubTmp;
	volatile uint32_t srcLaneIndexBuffer[wd::g_warpSize];
};

struct TransformSharedCount
{
	typename WarpReduce::TempStorage cubTmp;
};

// Buffer manager for two pass implemetnation.
// TODO: merge and make more-better...
class BufferManagerTp
{
public:
	uint32_t *m_countBuffer;/**< Buffer that is large enough to store both use cases of ordered*/
	uint32_t *m_offsetBuffer;/**< Buffer used to store offsets for each processor */
	uint32_t *m_globalCountBuffer;/**< Buffer that is large enough to store both use cases of ordered*/
	uint32_t *m_completionCountBuffer;/**< used to track the completion of warps to be able to perform operations once the last has finished*/
	int m_currentCountSlot;

	BufferManagerTp()
	{
		cudaMalloc(&m_countBuffer, sizeof(uint32_t) * (g_prefixBufferSize + 1));
		cudaMalloc(&m_offsetBuffer, sizeof(uint32_t) * (g_prefixBufferSize + 1));
		cudaMalloc(&m_globalCountBuffer, sizeof(uint32_t) * 2);
		cudaMemset(m_globalCountBuffer, 0, sizeof(uint32_t) * 2);
		cudaMalloc(&m_completionCountBuffer, sizeof(uint32_t) * 2);
		cudaMemset(m_completionCountBuffer, 0, sizeof(uint32_t) * 2);
		m_currentCountSlot = 0;
	}

	static BufferManagerTp& instance()
	{
		static BufferManagerTp inst;
		return inst;
	}

private:
	// No copying please!
	BufferManagerTp(const BufferManagerTp&) { }
};



struct UnOrderedOutputSequencer
{
	uint32_t * __restrict__ countPointer;
	uint32_t * __restrict__ nextCountPointer;
	uint32_t * __restrict__ completionCountPtr;
	uint32_t * __restrict__ nextCompletionCountPtr;
	uint32_t *__restrict__ m_totalPtr;
	//UnOrderedOutputSequencer()  = default;

	template <typename IN_START_FN_T, typename IN_END_FN_T, typename COUNT_PRED_T>
	__host__ void init(IN_START_FN_T inStartFn, IN_END_FN_T inEndFn, COUNT_PRED_T countPred, BufferManagerTp &bm, uint32_t *totalPtr)
	{
		countPointer = bm.m_globalCountBuffer + bm.m_currentCountSlot;
		completionCountPtr = bm.m_completionCountBuffer + bm.m_currentCountSlot;
		// Toggle the slot to use
		bm.m_currentCountSlot = 1 - bm.m_currentCountSlot;
		// We pass along the pointer to the other buffer too, so we can clear it.
		nextCountPointer = bm.m_globalCountBuffer + bm.m_currentCountSlot;
		nextCompletionCountPtr = bm.m_completionCountBuffer + bm.m_currentCountSlot;
		m_totalPtr = totalPtr;
	}

	__device__ void loadInitialWarpOffset(uint32_t &outOffset) const
	{
		// This just clears the counter for the next call, which saves having to launch an extra kernel just to do this
		// Mayhaps this is not a great idea. I'm not sure. The fact that we use an external buffer means it is not reentrant/multithreadable anyway.
		if (wd::laneId() == 0 && wd::globalWarpId() == 0)
		{
			*nextCompletionCountPtr = 0;
			*nextCountPointer = 0;
		}
		outOffset = 0;
	}

	__device__ void advanceGlobalWarpOffset(uint32_t &outOffset, uint32_t currentIterationTotal) const
	{
		if (wd::laneId() == 0)
		{
			outOffset = atomicAdd(countPointer, currentIterationTotal);
		}
		outOffset = __shfl_sync(0xffffffff, outOffset, 0);
	}

	/**
	* Does nothing in this case as the global is updated in the previous function.
	*/
	__device__ void advanceLocalOffset(uint32_t &outOffset, uint32_t currentIterationTotal) const
	{
	}

	// Only call if pointer is valid
	void storeTotal(uint32_t *totalPtr)
	{
		// This is maybe not madly efficient, could be done in main kernel for the other case, not sure how to roll that in neatly...
		//cudaMemcpy(totalPtr, countPointer, sizeof(uint32_t), cudaMemcpyDeviceToDevice);
	}


	__device__ void chunkCompleted(uint32_t totalProcessors) const
	{
		if (wd::laneId() == 0 && m_totalPtr)
		{
			uint32_t numCompleted = atomicAdd(completionCountPtr, 1);
			// If this was the last one to complete:
			if (numCompleted == totalProcessors - 1U)
			{
				*m_totalPtr = *countPointer;
			}
		}
	}

};


// Helper to perform the offset calculation before the final pass. Uses CUB to do prefix sum in a simgle kerne;s
template <int NUM, int THREADS>
__global__ void compactPrefixKernel(const uint32_t *__restrict__ input, uint32_t *__restrict__ output, uint32_t *__restrict__ totalPtr)
{
	// Uses CUBs block primitives to perform a prefix sum.
	const int elemsPerThread = (NUM + THREADS - 1) / THREADS;

	typedef cub::BlockScan<uint32_t, THREADS> BlockScan;
	typedef cub::BlockLoad<uint32_t, THREADS, elemsPerThread, cub::BLOCK_LOAD_WARP_TRANSPOSE> BlockLoad;
	typedef cub::BlockStore<uint32_t, THREADS, elemsPerThread, cub::BLOCK_STORE_WARP_TRANSPOSE> BlockStore;

	__shared__ union {
		typename BlockLoad::TempStorage load;
		typename BlockScan::TempStorage scan;
		typename BlockStore::TempStorage store;
	} tmp;

	uint32_t elements[elemsPerThread] = { 0 };

	BlockLoad(tmp.load).Load(input, elements);
	__syncthreads();
	uint32_t total = 0;
	BlockScan(tmp.scan).ExclusiveSum(elements, elements, total);
	__syncthreads();
	BlockStore(tmp.store).Store(output, elements);
	__syncthreads();

	if (threadIdx.x == 0)
	{
		*totalPtr = total;
	}
}

// Helper function that invokes the prefix sum kernel for global offset calc.
static void compactPrefix(const uint32_t *__restrict__ input, uint32_t *__restrict__ output, uint32_t *__restrict__ total)
{
	// Single block kernel as job is small enough that using off-the shelf device-wide compaction would waste a lot of time with sync etc.
	compactPrefixKernel<g_prefixBufferSize, 512><<<1, 512>>>(input, output, total);
}

struct OrderedOutputSequencer
{
	uint32_t *__restrict__ m_warpCounts;
	uint32_t *__restrict__ m_warpOffsets;
	// These are used to store and maintain the total count (result of the prefix sum)
	uint32_t * __restrict__ m_countPointer;
	uint32_t * __restrict__ m_nextCountPointer;


	template <typename IN_START_FN_T, typename IN_END_FN_T, typename COUNT_PRED_T>
	__host__ void init(IN_START_FN_T inStartFn, IN_END_FN_T inEndFn, COUNT_PRED_T countPred, BufferManagerTp &bm, uint32_t *totalPtr)
	{
		m_warpCounts = bm.m_countBuffer;
		m_warpOffsets = bm.m_offsetBuffer;

		m_countPointer = bm.m_globalCountBuffer + bm.m_currentCountSlot;
		// Toggle the slot to use
		bm.m_currentCountSlot = 1 - bm.m_currentCountSlot;
		// We pass along the pointer to the other buffer too, so we can clear it.
		m_nextCountPointer = bm.m_globalCountBuffer + bm.m_currentCountSlot;

		// make local copies for make benefit of lambda capture
		uint32_t *__restrict__ warpCounts = m_warpCounts;
		uint32_t *__restrict__ warpOffsets = m_warpOffsets;
		uint32_t * __restrict__ countPointer = m_countPointer;
		uint32_t * __restrict__ nextCountPointer = m_nextCountPointer;

		// Pass #1, counting
		wd::dispatchChunksRangeShared<TransformSharedCount>([=] __device__() { return inEndFn() - inStartFn(); }, [=] __device__(uint32_t chunkRangeStart, uint32_t chunkRangeEnd, TransformSharedCount &sh)
		{
			SizeType inStart = inStartFn();
			// clear the next count pointer.
			if (wd::laneId() == 0 && wd::globalWarpId() == 0)
			{
				*nextCountPointer = 0;
			}
			// Note, this must be done in the kernel as we support indirect range size for both input and output ranges
			//       they may thus fetch the size from a device side variable.
			const uint32_t length = inStart - inEndFn();
			//printf("Size: %d\n", length);
			int totalOutputCount = 0;

			for (int chunkIndex = chunkRangeStart; chunkIndex < chunkRangeEnd; chunkIndex += 1)
			{
				int i = chunkIndex * wd::g_warpSize + wd::laneId();

				bool isValid = i < length;
				uint32_t count = isValid ? countPred(i + inStart) : 0U;

				totalOutputCount += count;
			}

			totalOutputCount = WarpReduce(sh.cubTmp).Sum(totalOutputCount);

			if (wd::laneId() == 0)
			{
				warpCounts[wd::globalWarpId()] = totalOutputCount;
			}
		});

		// Prefix sum to find output offsets for each warp
		compactPrefix(warpCounts, warpOffsets, countPointer);
	}

	__device__ void loadInitialWarpOffset(uint32_t &outOffset) const
	{
		if (wd::laneId() == 0)
		{
			outOffset = m_warpOffsets[wd::globalWarpId()];
		}
		outOffset = __shfl_sync(0xffffffff, outOffset, 0);
	}

	/**
	* Does nothing in this case as the global offsets are pre-calculated in the build phase.
	*/
	__device__ void advanceGlobalWarpOffset(uint32_t &outOffset, uint32_t currentIterationTotal) const
	{
	}

	__device__ void advanceLocalOffset(uint32_t &outOffset, uint32_t currentIterationTotal) const
	{
		outOffset += currentIterationTotal;
	}
	// Only call if pointer is valid
	void storeTotal(uint32_t *totalPtr)
	{
		// This is maybe not madly efficient, could be done in main kernel for the other case, not sure how to roll that in neatly...
		cudaMemcpy(totalPtr, m_countPointer, sizeof(uint32_t), cudaMemcpyDeviceToDevice);
	}
	__device__ void chunkCompleted(uint32_t totalProcessors) const
	{
	}
};



// Shared implementation between base two-pass and unordered, not intended for external use
template <typename IN_START_FN_T, typename IN_END_FN_T, typename COUNT_PRED_T, typename EXP_PRED_T, typename OUTSEQ_T>
void transform_gen(IN_START_FN_T inStartFn, IN_END_FN_T inEndFn, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
{
	// TODO: add guard against re-entry, set a flag or something, for debug purposes.
	using namespace detail;
	// get from config, whatsmicallit...
	typedef OUTSEQ_T OutSeq;

	// set up with default buffer manager, in the case of ordered output, this also builds the count and offset tables
	// for use in the last pass. The buffer manager should be made a parameter somehow.
	OutSeq outSeq;
	outSeq.init(inStartFn, inEndFn, countPred, detail::BufferManagerTp::instance(), resultCount);

	const uint32_t invalidValue = 0xFFFFFFFF;
	// Pass #3 (or #1 in case of unordered), where actual moving takes place.
	wd::dispatchChunksRangeShared<TransformShared>([=] __device__() { return inEndFn() - inStartFn(); }, [=] __device__(uint32_t chunkRangeStart, uint32_t chunkRangeEnd, TransformShared &sh)
	{
		// Note, this must be done in the kernel as we support indirect range size for both input and output ranges
		//       they may thus fetch the size from a device side variable.
		SizeType inStart = inStartFn();
		SizeType inEnd = inEndFn();
		//const uint32_t length = inEnd - inStart;

		// 1. load and broadcast the output offset for the warp to all lanes
		uint32_t outOffset = 0;
		outSeq.loadInitialWarpOffset(outOffset);

		// Iterate over input all chunks
		for (uint32_t baseInIndex = chunkRangeStart * wd::g_warpSize; baseInIndex < chunkRangeEnd * wd::g_warpSize; baseInIndex += wd::g_warpSize)
		{
			//if (wd::laneId() == 0)
			//{
			//	printf("X %d\n", baseInIndex);
			//}
			uint32_t i = inStart + baseInIndex + wd::laneId();

			bool isValid = i < inEnd;
			// 1. count again (if the counting is actually expensive, then store and reload count, perhaps packed ones)
			uint32_t count = isValid ? countPred(i) : 0U;

			// 4. generate output items 
			uint32_t offset;
			uint32_t totalOutputCount;
			WarpScan(sh.cubTmp).ExclusiveSum(count, offset, totalOutputCount);

			// This call enables using an atomic operation to switch the behaviour of the implementation between ordered and unordered
			outSeq.advanceGlobalWarpOffset(outOffset, totalOutputCount);

			int outIntervalStart = int(offset);
			// Setting the output interval to negative simplifies the test in the loop
			int outIntervalStop = count ? outIntervalStart + int(count) - 1 : -1;

			for (uint32_t outIndex = wd::laneId(); outIndex < totalOutputCount + wd::laneId(); outIndex += wd::g_warpSize)
			{
				// prep buffer with invalid values...
				sh.srcLaneIndexBuffer[wd::laneId()] = invalidValue;

				// Write the (input) lane index to the end of the interval in the shared buffer
				if (outIntervalStop >= 0 && outIntervalStart < wd::g_warpSize)
				{
					// clamp if crossing the boundary
					sh.srcLaneIndexBuffer[min(int(wd::g_warpSize) - 1, outIntervalStop)] = wd::laneId();
				}

				// reverse prefix computes the input index for each SIMD lane
				int srcLaneIndex = reverse_warp_scan(sh.srcLaneIndexBuffer[wd::laneId()],
					[=](uint32_t v, uint32_t r, bool inRange)
				{
					if (v == invalidValue)
					{
						return r;
					}
					return v;
				}
				);

				uint32_t subIndex = outIndex - __shfl_sync(0xffffffff, offset, srcLaneIndex);
				uint32_t srcIndex = inStart + baseInIndex + srcLaneIndex;
				if (outIndex < totalOutputCount)
				{
					assert(srcIndex < inEnd);

					expPred(srcIndex, outOffset + outIndex, subIndex);
				}

				outIntervalStart -= wd::g_warpSize;
				outIntervalStop -= wd::g_warpSize;
			}
			// this call does nothing in the case of the atomic versionm but advances the offset locally otherwise
			outSeq.advanceLocalOffset(outOffset, totalOutputCount);
		}

		outSeq.chunkCompleted(wd::g_numProcessors);
	});

	if (resultCount)
	{
		outSeq.storeTotal(resultCount);
	}
}


} // namespace detail



/**
*/
//template <typename INPUT_RANGE_T, typename OUTPUT_RANGE_T, typename COUNT_PRED_T, typename EXP_PRED_T>
//void transform_tp(const INPUT_RANGE_T &inputRange, OUTPUT_RANGE_T &outputRange, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
//{
//	detail::transform_gen<INPUT_RANGE_T, OUTPUT_RANGE_T, COUNT_PRED_T, EXP_PRED_T, detail::OrderedOutputSequencer>(inputRange, outputRange, countPred, expPred, resultCount);
//}
//

/**
 */
template <typename IN_START_FN_T, typename IN_END_FN_T, typename COUNT_PRED_T, typename EXP_PRED_T, typename TT = typename std::enable_if<std::is_convertible<IN_START_FN_T, std::function<SizeType()>>::value>::type>
inline void transform_uo(IN_START_FN_T inStartFn, IN_END_FN_T inEndFn, COUNT_PRED_T countPred, EXP_PRED_T expPred, SizeType *resultCount)
{
	detail::transform_gen<IN_START_FN_T, IN_END_FN_T, COUNT_PRED_T, EXP_PRED_T, detail::UnOrderedOutputSequencer>(inStartFn, inEndFn, countPred, expPred, resultCount);
}

}; // namespace stream_transform


#endif // __CUDACC__


#endif // _stream_transform_tp_h_
