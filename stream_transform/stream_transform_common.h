/****************************************************************************/
/* Copyright (c) 2018, Ola Olsson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/****************************************************************************/
#ifndef _stream_transform_common_h_
#define _stream_transform_common_h_


#if defined(_MSC_VER) && _MSC_VER <= 1900
// Note: this warning is removed in Visual C++ 2017 and does not affect correctness of the program
//       it is to do with the length of symbols and might affect debugging.
#pragma warning(disable:4503)
#endif // defined(_MSC_VER) && _MSC_VER <= 1900

#include <inttypes.h>
#include <assert.h>
#include <iterator>

#ifdef __CUDACC__
#include <cub/warp/warp_reduce.cuh>
#include <cub/warp/warp_scan.cuh>
#include <cub/block/block_scan.cuh>
#include <cub/block/block_load.cuh>
#include <cub/block/block_store.cuh>

#define STFM_CUDA_FN_HD __device__ __host__
#define STFM_CUDA_FN_D __device__
#define STFM_CUDA_FN_H __host__

#define STFM_LAMBDA __device__

#else // !__CUDACC__

#define STFM_CUDA_FN_HD 
#define STFM_CUDA_FN_D 
#define STFM_CUDA_FN_H 

#define STFM_LAMBDA 

#endif // __CUDACC__


namespace stream_transform
{

namespace compilation_context
{
	struct cuda
	{
	};

	struct host
	{
	};
#ifdef __CUDACC__
	namespace compilation_context
	{
		using default_context = cuda;
	}
#else // __CUDACC__
	namespace compilation_context
	{
		using default_context = host;
	}
#endif // __CUDACC_
}
// Only allow pointer differences of up to 4 Gigathings, plenty for GPU and saves registers and operaions (hopefully).
// TODO: make this possible to change at compile-time?
typedef uint32_t SizeType;

namespace detail
{
#ifdef __CUDACC__
	

typedef cub::WarpScan<uint32_t> WarpScan;
typedef cub::WarpReduce<int> WarpReduce;


#endif // __CUDACC__

} // namespace detail


// TODO: move to own file?
namespace range
{

	template <typename T>
	class PointerRange
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;

		STFM_CUDA_FN_HD iterator begin() { return m_pointer; }
		STFM_CUDA_FN_HD iterator end() { return m_pointer + m_size; }
		STFM_CUDA_FN_HD const_iterator begin() const { return m_pointer; }
		STFM_CUDA_FN_HD const_iterator end()  const { return m_pointer + m_size; }
		STFM_CUDA_FN_HD SizeType size() const { return m_size; }
	public:
		T *m_pointer;
		SizeType m_size;
	};



	template <typename T>
	inline PointerRange<T> pointer(T *pointer, SizeType count)
	{
		PointerRange<T> r;
		r.m_pointer = pointer;
		r.m_size = count;
		return r;
	}



	template <typename T>
	class PointerRangeIndirect
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;

		STFM_CUDA_FN_HD iterator begin() { return m_pointer; }
		STFM_CUDA_FN_HD iterator end() { return m_pointer + *m_sizePtr; }
		STFM_CUDA_FN_HD const_iterator begin() const { return m_pointer; }
		STFM_CUDA_FN_HD const_iterator end()  const { return m_pointer + *m_sizePtr; }
		STFM_CUDA_FN_HD SizeType size() const { return *m_sizePtr; }
	public:
		T *m_pointer;
		const SizeType *m_sizePtr;
	};



	/**
	 * Both pointer must point to the same address space (or unified). The value pointed to by countPtr is not
	 * accessed until the start of the kernels, that is not ever on the host side. It is the responsibility of the
	 * user to ensure proper synchronization, such that the kernel that produces the value has completed before
	 * the consumer starts, and so on. This ability is provided by the warp dispatcher, since it always launches a
	 * number of warps that depend on the device, not the input size. Note that a value of 0 will thus not  cancel
	 * the kernel launch, but it will exit as soon as this is detected.
	 */
	template <typename T>
	PointerRangeIndirect<T> indirect(T *pointer, const SizeType *countPtr)
	{
		PointerRangeIndirect<T> r;
		r.m_pointer = pointer;
		r.m_sizePtr = countPtr;
		return r;
	}

	template <typename T>
	class IteratorRange
	{
	public:
		typedef T iterator;
		typedef T const_iterator;

		typedef typename std::iterator_traits<T>::value_type value_type;
		typedef typename std::iterator_traits<T>::reference reference;

		STFM_CUDA_FN_HD iterator begin() { return m_first; }
		STFM_CUDA_FN_HD iterator end() { return m_last; }
		STFM_CUDA_FN_HD const_iterator begin() const { return m_first; }
		STFM_CUDA_FN_HD const_iterator end()  const { return m_last; }
		STFM_CUDA_FN_HD SizeType size() const { return m_last-m_first; }

	public:
		T m_first;
		T m_last;
	};


	template <typename T>
	IteratorRange<T> ierator(T first, T last)
	{
		IteratorRange<T> r;
		r.m_first = first;
		r.m_last = last;
		return r;
	}

} // namespace range

}; // namespace stream_transform


#endif // _stream_transform_common_h_
